<nav class="navbar navbar-default" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">

        <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          <!--a class="navbar-brand" href="#"><img style="height:33px;" src="/assets/img/noctula.jpg" /></a>-->
      </div>    
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" ng-class="!navCollapsed && 'in'" ng-click="navCollapsed=true">
      
        <ul class="nav navbar-nav">
          <li><a ui-sref="home">Home</a></li>
          <li><a ui-sref="jobsMy">My Jobs <span class="badge">5</span></a></li>
          <li><a ui-sref="jobCreate">New Job</a></li>
          <li><a ui-sref="jobGroupCreate">New Job Group</a></li>
          <li><a ui-sref="users">Users</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#">Outro</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle">{{userDash.avatarUri}} <img src="https://yt3.ggpht.com/-dAk55ZJbXsQ/AAAAAAAAAAI/AAAAAAAAAAA/KVMnwyTjLw4/s88-c-k-no/photo.jpg" class="user_avatar" /> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a ui-sref="userSettings">Settings</a></li>
              <li><a ui-sref="accountCredentials">Credentials</a></li>
              <li><a ui-sref="profile">My Profile</a></li>
              <li><a ng-click="logout()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div><!-- /.navbar-collapse -->
    </nav>