/* 
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */

/**
 * Directive for creating bids
 * 
 * This directive can be applied to buttons, images, etc
 */
angular.module('jobsApp')
        .directive('bidCreate', function($modal){
            return {
                restrict: 'EAC',
                scope: {
                    
                },
                link: function(scope, element, attrs) {
                    var auctionId = attrs.auctionId;
                    element.bind('click', function(){
                        var modalInstance = $modal.open({
                           templateUrl: '/partials/bidCreate.html',
                           controller: 'bidCreateCtrl',
                           resolve: {
                               auctionId: function(){
                                   return auctionId;
                               }
                           }
                        });
                        modalInstance.result.then(function(res) {
                            
                        }, function(res) {
                            
                        });
                    });
                }
            } 
        }).controller('bidCreateCtrl', ['$rootScope', '$scope', '$modalInstance', 'auctionId', 'jobSvc', function($rootScope, $scope, $modalInstance, auctionId, jobSvc) {
                
                jobSvc.getById(auctionId).then(function(res) {
                    $scope.auctionName = res.name;
                })
                
                $scope.ok = function(){
                    $modalInstance.close();
                };
                
                $scope.cancel = function(){
                    $modalInstance.dismiss();
                };
        }]);

