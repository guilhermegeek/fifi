/* 
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */


angular.module('jobsApp').directive('auctionDisplay', ['jobSvc', '$sce', function(jobSvc, $sce){
    return {
        templateUrl: '/partials/auctionDisplay.html',
        /**
         * Don't replace because grid classes and others directivesd would be loosed
         */
        replace: false,
        scope: {
            auction: '='
        },
        link: function(scope, element, attrs) {
            var auctionId = scope.auction.id,
                    trusted = {};
            scope.getContentHtml = function(html) {
                 return  trusted[html] || (trusted[html] = $sce.trustAsHtml(html));
            }
            
        }
    };
}]);