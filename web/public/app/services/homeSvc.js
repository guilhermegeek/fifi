angular.module("jobsApp").service('homeSvc', ['$http', '$q', function($http, $q) {
    return {
        getHome: function(){
            var defer = $q.defer();
            $http.get('/home.json')
                .success(function(res) {
                    return defer.resolve(res);
                }).error(function(res) {
                   return defer.reject(res); 
                });
            return defer.promise;
        }
    };
}]);