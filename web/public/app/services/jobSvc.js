function postJobReq() {
    this.name = '';
    this.content = '';
    this.minBudget = null;
    this.maxBudget = null;
    this.begin = null;
    this.end = null;
    this.categories = [];
}
/**
 * Job Service
 * 
 */
angular.module("jobsApp").service('jobSvc', ['$http', '$q', '$angularCacheFactory', 'Restangular', 'modalSvc', function($http, $q, $angularCacheFactory, Restangular, modalSvc) {

        /**
         * job collection cache
         * 
         * Register a cache for jobs
         * All API calls will use this cache first
         */
        $angularCacheFactory('jobCache', {
            onExpire: function(key, value) {
                $http.get(key).success(function(res) {
                    jobCache.put(key, res);
                })
            }

        });
        var jobSvc = {};
        jobSvc.getAuction = function(id) {
            var defer = $q.defer();

            var job = Restangular.one('jobs', id)
                    .get();
            defer.resolve(job);

            return defer.promise;
        };
        /**
         * 
         * @param {type} id
         * @returns {unresolved}Basic Auction information
         */
        jobSvc.getInfo = function(id) {
            var defer = $q.defer();

            var info = Restangular.one('jobs', id).get();
            defer.resolve(info);

            return defer.promise;
        };
        /**
         * return a job 
         * 
         * Return a job dy id
         * @param {string} id
         * @returns job dto
         */
        jobSvc.getById = function(id) {
            var deferred = $q.defer(),
                    jobCache = $angularCacheFactory.get('jobCache');

            if (jobCache.get(id)) {
                deferred.resolve(jobCache.get(id));
            }
            else {
                var job = Restangular.one('jobs', id)
                        .get();
                deferred.resolve(job);
                /*$http.get('/api/jobs/' + id).success(function(res) {
                 deferred.resolve(res);
                 });*/
            }
            return deferred.promise;
        };
        /**
         * return jobs
         * 
         * Get a list of jobs filtered
         * @returns array
         */
        jobSvc.getAll = function(queries) {
            var defer = $q.defer(),
                    jobCache = $angularCacheFactory.get('jobCache');
            if (jobCache.keys().length > 0) {
                defer.resolve(jobCache.keys())
            }
            else {
                Restangular.all('jobs', queries).getList()
                        .then(function(res) {
                            // Save results in cache
                            /*for(var i = 0; i < res.length; i++) {
                             jobCache.put(res[i].id, res[i]);
                             }*/
                            defer.resolve(res);
                        });
            }
            return defer.promise;
        };
        jobSvc.getAuctionsByOwner = function() {
            var defer = $q.defer();
            Restangular.all('me/jobs').getList()
                    .then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    });
            return defer.promise;
        };
        jobSvc.getGroupsByOwner = function() {
            var defer = $q.defer();
            Restangular.all('me/groups').getList()
                    .then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    })
            return defer.promise;
        };
        /**
         * create a new Job
         * 
         * Create a new Job without publishing it
         * Can be a single Job or a Workspace with multiple Jobs
         * 
         * @param string name The title
         * @param string content Description using html
         * @param array jobs List of Jobs
         * @returns array of Job dto
         */
        jobSvc.createGroup = function(name, content) {
            var defer = $q.defer(),
                    jobCache = $angularCacheFactory.get('jobCache');

            $http.post('/api/group', {name: name, content: content})
                    .then(function(res) {
                        jobCache.put(res.id, res);
                        return defer.resolve(res);
                    }, function(res) {
                        return defer.reject(res);
                    });
            return defer.promise;
        };
        jobSvc.createJob = function(name, content, begin, end, minBudget, maxBudget, payment) {
            var defer = $q.defer(),
                    jobCache = $angularCacheFactory.get('jobCache');
            var postJobReq = {
                name: name,
                content: content,
                begin: begin,
                end: end,
                minBudget: minBudget,
                maxBudget: maxBudget,
                payment: payment
            };
            var res = Restangular.all('jobs').post(postJobReq).then(function(res) {
                //jobCache.put(res.id, res);
                defer.resolve(res);
            });
            return defer.promise
        };

        jobSvc.createAuctionForGroup = function(groupId, name, content, begin, end, minBudget, maxBudget, payment) {
            var defer = $q.defer(),
                    jobCache = $angularCacheFactory.get('jobCache');

            var req = {
                groupId: groupId,
                name: name,
                content: content,
                begin: begin,
                end: end,
                minBudget: minBudget,
                maxBudget: maxBudget,
                payment: payment
            };

            Restangular.all('jobs').post(req).then(function(res) {
               // jobCache.put(res.id, res);
                defer.resolve(res);
            });

            return defer.promise;
        }
        /**
         * Update Auth
         */
        jobSvc.saveAuction = function(id, name, content) {
            var defer = $q.defer();
            var putAuctionReq = {
                name: name,
                content: content
            };

            Restangular.oneUrl('jobs/' + id + '/info')
                    //.withHttpConfig({ transformRequest: angular.identity })
                    .customPOST(putAuctionReq, '').then(function(res) {
                modalSvc.display(res);
                defer.resolve(res);
            }, function(res) {
                modalSvc.display(res);
                defer.reject(res);
            });
            return defer.promise;
        };

        /**
         * Controllers
         */
        jobSvc.auctionCreateCtrl = function() {
            var defer = $q.defer();
            var res = Restangular.one('job/create.json').get();
            defer.resolve(res);
            return defer.promise;
        };
        return jobSvc;
    }]);
