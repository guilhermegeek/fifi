angular.module('jobsApp').service('welcomeSvc', ['Restangular', '$http', '$q', function(Restangular, $http, $q) {
        return {
            getWelcomeCtrl: function(){
                var defer = $q.defer();
                $http.get('/welcome.json').then(function(res) {
                    defer.resolve(res);
                }, function(res) {
                    defer.reject(res);
                });
                return defer.promise;
            }
        }
}]);    