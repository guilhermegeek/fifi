/**
 * Modal Service
 * 
 * This services handles all http responses
 * For now they're displayed in a Modal but others implementations should be done here without affecting all the consumers
 */

var jobsApp = angular.module("jobsApp");

/**
 * The Service
 */
jobsApp.service('modalSvc', ['$modal', function($modal) {
        var modalSvc = {};
        var addModal = function(title, message) {
          var instance = $modal.open({
                templateUrl: 'modalDisplay.html',
                controller: 'modalDisplayCtrl',
                resolve: {
                    modalObj: function(){
                        return  {
                            title: title,
                            body: message
                        };
                    }
                }
            });  
        };
        modalSvc.display = function(response){
            var message = response.data === undefined ? response.message : response.data.message;
            addModal('Obrigado!', message);         
        };
        modalSvc.success = function(title, message) {
            addModal(title, message);
        }
        modalSvc.internalError = function(response) {
          addModal('Ups', 'Sorry but something went wront');
        };
        return modalSvc;
}]);
/**
 * Modal Controller
 */
jobsApp.controller('modalDisplayCtrl', ['$scope', '$modalInstance', 'modalObj', function($scope, $modalInstance, modalObj) {
        $scope.title = modalObj.title;
        $scope.body = modalObj.body;
        
    $scope.ok = function(){
        $modalInstance.dismiss('cancel');
    };
    $scope.cancel = function(){
        $modalInstance.close();
    };
}]);