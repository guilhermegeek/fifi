angular.module("jobsApp").service('accountSvc', ['$http', '$q', 'Restangular', 'modalSvc', function($http, $q, Restangular, modalSvc) {
	return {
                /*
                 * Login Authentication
                 * Login is done one time, all others API calls are authorized with a token
                 */
		login: function(emailOrNick, pw){
                    var defer = $q.defer();
                    $http.post('/login', { email: emailOrNick, password: pw }, {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                        }).then(function(res) {
                                return defer.resolve(res);
                        }, function(res) {
                            return defer.reject(res);
                        });
                    return defer.promise;
		},
                /*
                 * Register a new account
                 */
		register: function(firstName, lastName, email, password, passwordConfirm) {
                    var defer = $q.defer();
                    $http.post('/register', { firstName: firstName, lastName: lastName, email: email, password: password, passwordConfirm: passwordConfirm})
                        .then(function(res){
                            return defer.resolve(res);
                        }, function(res) {
                            return defer.reject(res);
                        });
                    return defer.promise;
		},
                
                /**
                 * Update basic information
                 * @param ObjectId id
                 * @param string firstName
                 * @param string lastName
                 * @returns promise
                 */
                saveBasicInfo: function(firstName, lastName) {
                    var defer = $q.defer(),
                        req = { firstName: firstName, lastName: lastName};
                
                    var response = Restangular.one('account').one('info')
                            .customPOST(req, '').then(function(res) {
                                modalSvc.display(res);
                        defer.resolve(response);
                    }, function(res) {
                        modalSvc.display(res);
                        defer.reject(response);
                    });
                    
                    
                    return defer.promise;
                },
                getBasicInfo: function() {
                    var defer = $q.defer();
                    var response = Restangular.one('account').one('info').get();
                    defer.resolve(response);
                    return defer.promise;
                }
	}
}]);