var jobsApp = angular.module('jobsApp');

jobsApp.service('userSvc', ['Restangular', '$angularCacheFactory', '$q', function(Restangular, $angularCacheFactory, $q) {
        var userCache = $angularCacheFactory.get('userCache');
        
        $angularCacheFactory('userCache', {
            onExpire: function(key, value) {
                $http.get(key).success(function(res) {
                    userCache.put(key, res);
                });
            }
        });

        return {
          getAll: function(skip, take){
              var defer = $q.defer();
              if(userCache.keys().length > 0) {
                  defer.resolve(userCache.keys());
              }
              else {
                  Restangular.all('users').getList()
                          .then(function(res) {
                              defer.resolve(res);
                  })
              }
              return defer.promise;
          }  
        };
}]);