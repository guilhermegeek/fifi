angular.module('jobsApp').factory('jobCreateSvc',['modalSvc', 'jobSvc', '$upload', '$rootScope', function(modalSvc, jobSvc, $upload, $rootScope) {
        
        /**
         * The service instance
         */
        var jobCreateSvc = {};
        
        /**
         * Job types
         * 
         * @type object<array, object>
         */
        var jobs = {
          'ENVIRONMENT': [],
          'DESIGN' : []
        };
        
        /**
         * 
         * @param {type} jobsArray
         * @param {type} job
         * @returns Job DTO
         */
        var addJob = function(jobsArray, job) {
            if(!angular.isObject(job)){
                throw new Error("jobCreateSvc.addJob only accepts objects as job");
            }
            jobsArray.push(job);
            return job;
        }
        
        jobCreateSvc.getCurrent = function(){
          return [].contact(jobs.ENVIRONMENT, jobs.DESIGN)  
        };
        
        jobCreateSvc.addJobEnvironment = function(job) {
            job
          return addJob(jobs.ENVIRONMENT, job);  
        };
        
        jobCreateSvc.addDesign = function(job) {
          return addJob(jobs.ENVIRONMENT, job); 
        };
        
        return jobCreateSvc;
}]);