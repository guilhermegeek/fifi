# AngularJS Application

### Three
Folder three:
* controllers 
* services
* directives
* tests - The test environment is configured to look up for tests in this folder, and his sub folders
 * controllers
 * directives
 * services

## Dependencies
Here's the complete list of all angular and others dependencies
All handled with bower
* Restangular - Replaces the $http provider. Handles API request much more efficiently
* Lodash - Dependency of Restangular
* Angular Cache - Replaces $cacheFactory. Has much more features and it's better suited for Single Page Applications
* Angular DragDrop - Drag and drop directives and services to help implement drag and drop in directives
* Select2 - Library to enchant select elements. Has features like tags and dropdown
* Angular Ui Select2 - Angular directives for Select2
* Angular Ui Router - This application uses the Ui Router with the state pattern instead of url routing.
* textAngular - WYSIWYG editor. Was designed with plugins implementation in mind, so it's pretty easy to add a attachments or grid component