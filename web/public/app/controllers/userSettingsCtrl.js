angular.module("jobsApp").controller("userSettingsCtrl", ['$scope', 'accountSvc', 'modalSvc', function($scope, accountSvc, modalSvc) {
        $scope.basicInfo = {};
        accountSvc.getBasicInfo()
            .then(function(res){
                $scope.basicInfo.firstName = res.firstName;
                $scope.basicInfo.lastName = res.lastName;
            }, function(res) {
                
            });
        $scope.saveBasicInfo = function(){
          accountSvc.saveBasicInfo($scope.basicInfo.firstName, $scope.basicInfo.lastName)
                  .then(function(res) {
                      
                    }, function(res) {
                      
                    });
        };
}]);