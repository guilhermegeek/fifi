angular.module("jobsApp").controller('welcomeCtrl', 
['$scope', '$rootScope', 'accountSvc', 'welcomeSvc', 'modalSvc', function($scope, $rootScope, accountSvc, welcomeSvc, modalSvc) {
    /*
     * Registration
     */
    
    welcomeSvc.getWelcomeCtrl().then(function(res){
        $scope.totalRegistered = res.data.totalRegistered;
        $scope.totalJobsFinished = res.data.totalJobsFinished
    });
    
    $scope.newAccount = {};
    $scope.register= function(){
        var a = $scope.register;
        accountSvc.register(a.firstName, a.lastName, a.email, a.password, a.passwordConfirm)
                .then(function(res) {
                    modalSvc.display(res);
        }, function(res){
            modalSvc.display(res);
        });
    }
    $scope.login = function(){
      accountSvc.login($scope.login.email, $scope.login.password)
              .then(function(res){
                 window.location = "/home";
      }, function(res) {
          modalSvc.display(res); 
      });
    }
    
    $scope.jobs = [];
    $scope.jobs.push({
            id: 1,
            name: 'Serra do Caramulo',
            image: 'http://noctula.pt/wp-content/uploads/2014/02/parque_eolico_Estado_Bahia_Brasil_NOCTULA-455x205.jpg',
            category: 'Environment',
            content: '<p>Preciso de um website para um Restaurante.</p>',
            minBudget: 100,
            maxBudget: 200
    });
    $scope.jobs.push({
            id: 1,
            name: 'Serra da Estrela',
            image: 'http://noctula.pt/wp-content/uploads/2014/01/Fraga_da_Pena_Plano_Valorizacao_Ecologica_Sector_Turistico_NOCTULA-455x205.jpg',
            category: 'Environment',
            content: '<p>Preciso de um website para um Restaurante.</p>',
            minBudget: 100,
            maxBudget: 200
    });
    $scope.jobs.push({
            id: 1,
            name: 'Serra do Caramulo',
            image: 'http://noctula.pt/wp-content/uploads/2014/02/parque_eolico_Estado_Bahia_Brasil_NOCTULA-455x205.jpg',
            category: 'Environment',
            content: '<p>Preciso de um website para um Restaurante.</p>',
            minBudget: 100,
            maxBudget: 200
    });
}]);