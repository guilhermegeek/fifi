/**
 * Account credentials
 */
angular.module('jobsApp').controller('accountCredentialsCtrl', ['$scope', 'Restangular', function($scope, Restangular) {
    
        $scope.save = function(){
        var req = {
          currentPw: $scope.cred.currentPw,
          newPw: $scope.cred.newPw,
          newPwConfirm: $scope.cred.newPwConfirm
        };
    };    
}]);