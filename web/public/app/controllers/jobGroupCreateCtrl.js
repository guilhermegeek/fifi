angular.module("jobsApp").controller('jobGroupCreateCtrl', ['$scope', 'jobSvc', 'modalSvc', function($scope, jobSvc, modalSvc) {
    $scope.create = function(){
        if($scope.groupCreate.$valid) {
            jobSvc.createGroup($scope.group.name, $scope.group.content).
                    then(function(res) {
                        modalSvc.display(res);
            }, function(res) {
                modalSvc.display(res);
            })
        }
    };
}]);