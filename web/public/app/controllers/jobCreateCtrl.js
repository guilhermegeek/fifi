/**
 * Job create controller
 * 
 * Create new Job
 * @type controller
 */
var jobsApp = angular.module("jobsApp");

jobsApp.controller('jobCreateCtrl', ['$scope', '$rootScope', 'jobSvc', 'modalSvc', function($scope, $rootScope, jobSvc, modalSvc) {
    $scope.job = { id: '', name: '', content: ''};
    $scope.jobs = [];
    
    jobSvc.auctionCreateCtrl().then(function(res) {
       $scope.groups = res.groups; 
    }, function(res) {
        alert('error');
    });
    
    
    /**
     * textAngular options
     * 
     * Options for textAngular directive used in descriptions
     */
    $scope.orightml = '<h2>Try me!</h2><p>textAngular is a super cool WYSIWYG Text Editor directive for AngularJS</p><p><b>Features:</b></p><ol><li>Automatic Seamless Two-Way-Binding</li><li>Super Easy <b>Theming</b> Options</li><li style="color: green;">Simple Editor Instance Creation</li><li>Safely Parses Html for Custom Toolbar Icons</li><li class="text-danger">Doesn&apos;t Use an iFrame</li><li>Works with Firefox, Chrome, and IE8+</li></ol><p><b>Code at GitHub:</b> <a href="https://github.com/fraywing/textAngular">Here</a> </p>';
    $scope.htmlcontent = $scope.orightml;
    $scope.disabled = false;
    
    /**
     * Begin Date
     */
    $scope.beginOpened = false;
    $scope.beginOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.beginOpened = true;
    };
    
    /**
     * End Date
     */
    $scope.endOpened = false;
    $scope.endOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.endOpened = true;
    }
    
   
    /*jobSvc.getCreateCtrl().then(function(model) {
       $scope.categories = model.categories; 
    });*/
    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };


    /**
     * Functions to update global state of Job, according to sub jobs
     * Mostly sum operations
     */

    /**
     * Format a input to Budget
     * If is undefined, null or not a number returns 0
     */
    var formatBudget = function(ammount) {
        return (typeof(ammount) != "undefined" 
                && ammount != null
                && !isNaN(ammount))
        ? ammount : 0;
    };
    $scope.global = {
        minBudget: 0,
        maxBudget: 0
    };

    $scope.globalMinBudget = function(){
        var minBudget = 0;
        for(var i = 0; i < $scope.jobs.length; i++){
            minBudget += parseFloat(formatBudget($scope.jobs[i].minBudget));
        }
        $scope.global.minBudget = minBudget;
    };
    $scope.globalMaxBudget = function(){
      var maxBudget = 0;
        for(var i = 0; i < $scope.jobs.length; i++){
            maxBudget += parseFloat(formatBudget($scope.jobs[i].maxBudget));
        }
        $scope.global.maxBudget = maxBudget;  
    };
    
    
    /**
     * Create a new Job
     * The request send both JobGroup and Job
     */
    $scope.register = function(){
        jobSvc.createJobGroup($scope.job.name, $scope.job.content, $scope.jobs);
    };
    $scope.hasGroup = function(){
        return $scope.group != null && $scope.group != "";
    }
    $scope.create = function(){
        
      if($scope.jobCreate.$valid) {
          if($scope.hasGroup()) {
              jobSvc.createAuctionForGroup($scope.group.id, $scope.job.name, $scope.job.content, $scope.job.begin, $scope.job.end, $scope.job.minBudget, $scope.job.maxBudget, 'visa')
                    .then(function(res) {
                        modalSvc.display(res.data);
            }, function(res) {
                modalSvc.display(res);    
            });
          } else {
            jobSvc.createJob($scope.job.name, $scope.job.content, $scope.job.begin, $scope.job.end, $scope.job.minBudget, $scope.job.maxBudget, 'visa')
                    .then(function(res) {
                        modalSvc.display(res.data);
            }, function(res) {
                modalSvc.display(res);    
            });
        }
      }  
    };
    
    // Initial tab
    //$scope.addTab();
}]);

jobsApp.controller('jobCreateWorkspaceCtrl', ['$scope', 'jobSvc', 'modalSvc', 'Restangular', function($scope, jobSvc, modalSvc, Restangular) {
    $scope.createWorkspace = function(){
      jobSvc.createWorkspace($scope.workspace.name, $scope.workspace.content)
              .then(function(res) {
                  modalSvc.display(res);
                  
      }, function(res) {
          modalSvc.display(res);
      });
    };
}]);