angular.module("jobsApp").controller('jobEditCtrl', ['$scope', 'jobSvc', '$stateParams', function($scope, jobSvc, $stateParams) {
        var auctionId = $stateParams.id;
        jobSvc.getInfo(auctionId).then(function(res) {
            $scope.job = res;
        }, function(){
           alert('error'); 
        });
        $scope.save = function(){
            jobSvc.saveAuction(auctionId, $scope.job.name, $scope.job.content);
        }
}]);