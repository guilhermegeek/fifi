// View Job controller
angular.module("jobsApp").controller('jobCtrl', ['$scope', '$rootScope', 'jobSvc', '$stateParams', function($scope, $rootScope, jobSvc, $stateParams) {
	jobSvc.getAuction($stateParams.id).then(function(res) {
           $scope.name = res.name;
            $scope.content = res.content;
            $scope.bids = res.bids;
            $scope.minBudget = res.minBudget;
            $scope.maxBudget = res.maxBudget;
            $scope.minBid = res.minBid;
            $scope.maxBid = res.maxBid
            $scope.comments = res.comments;
            $scope.rating = res.rating; 
        });
	
}]);