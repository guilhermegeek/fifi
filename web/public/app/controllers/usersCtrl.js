/**
 * Users Dashboard
 *
 * Controller for users management.
 */

var jobsApp = angular.module('jobsApp');

jobsApp.controller('usersCtrl', ['$scope', '$rootScope', 'Restangular', 'modalSvc', 'userSvc', function($scope, $rootScope, Restangular, modalSvc, userSvc) {
	userSvc.getAll(0, 100).then(function(res) {
           $scope.users = res;
           
        }, function(){
            
        });
}]);

