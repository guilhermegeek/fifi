/**
 * Job view controller
 *
 */
var jobsApp = angular.module('jobsApp');
jobsApp.controller('jobCtrl', ['$scope', '$rootScope', '$stateParams', 'Restangular', 'jobSvc', 'modalSvc', '$sce', function($scope, $rootScope, $stateParams, Restangular, jobSvc, modalSvc, $sce) {
	
	jobSvc.getAuction($stateParams.id).then(function(res) {
           $scope.name = job.name;
            $scope.content = $sce.trustAsHtml(job.content);
            $scope.bids = job.bids;
            $scope.minBudget = job.minBudget;
            $scope.maxBudget = job.maxBudget;
            $scope.minBid = job.minBid;
            $scope.maxBid = job.maxBid
            $scope.comments = job.comments;
            $scope.rating = job.rating; 
        });
	

	/**
	 * Bid Proposal
	 *
	 * Send a bid proposal for the job
	 */
	$scope.bid = function(ammount) {
		var res = Restangular.one('bid').post();
		modalSvc.display(res);
	};

	/**
	 * New Comment
	 *
	 * Create a new comment
	 */
	$scope.comment = function(){
		var res = Restangular.one('job', id).one('comment').post();
		modalSvc.display(res);
	}
}]);