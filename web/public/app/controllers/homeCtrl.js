// Home controller
angular.module("jobsApp").controller('homeCtrl', ['$scope', '$rootScope', 'homeSvc', 'jobSvc', '$sce', function($scope, $rootScope, homeSvc, jobSvc, $sce) {
    $scope.jobs = [];
    $scope.searchModel = {};
    $scope.categories = ['Ambiente', 'Web Design', 'Ambiente #2', 'Ambiente Moderno'];
    $scope.searchOptions = {
        //'multiple': true,
        //'simple_tags': true
       // 'tags': ['Ambiente', 'Web Design', 'Ambiente #2', 'Ambiente Moderno']  // Can be empty list.
    };
    jobSvc.getAll().then(function(res) {
        $scope.jobs = res || [];
    }, function(res) {
        alert('error');
    });
    var trusted = {};
    $scope.getContentHtml = function(html) {
      return  trusted[html] || (trusted[html] = $sce.trustAsHtml(html));
    };
}]);