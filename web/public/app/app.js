var jobsApp =  angular.module("jobsApp", ['angularFileUpload', 'jmdobry.angular-cache', 'restangular', 'ui.select2', 'ui.bootstrap', 'ui.router', 'ngSanitize', 'ngRoute', 'textAngular', 'mgo-angular-wizard', 'ngTouch']);

/**
 * Module configuration
 * 
 * Configuration of all dependencies and the module itself
 */
jobsApp.config(['$stateProvider', '$routeProvider', '$locationProvider', '$urlRouterProvider', '$httpProvider', '$angularCacheFactoryProvider', 'RestangularProvider', function($stateProvider, $routeProvider, $locationProvider, $urlRouterProvider, $httpProvider, $angularCacheFactoryProvider, RestangularProvider) {
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        
        /**
         * Requests as AJAX
         * 
         * Identify all http requests as ajax, setting the X_REQUESTED_WITH header readed by most APIs
         */
        $httpProvider.defaults.headers.common['X_REQUESTED_WITH'] = 'XMLHttpRequest';
        
        /**
         * Route states
         * 
         * All application route states
         * This has anything to do with the API
         */
        $stateProvider
		.state('welcome', {
                    url: '/welcome',
                    templateUrl: '/partials/welcome.html',
                    controller: 'welcomeCtrl'
		}).state('home', {
                    url: '/home',
                    templateUrl: '/partials/home.html',
                    controller: 'homeCtrl'
                }).state('faq', {
                    url: '/faq',
                    templateUrl: '/partials/faq.html',
                    controller: 'faqCtrl'
		}).state("jobCreate", {
                    url: '/job/create',
                    templateUrl: '/partials/jobCreate.html',
                    controller: 'jobCreateCtrl'
		}).state('jobGroupCreate', {
                    url: "/job/group/create",
                    templateUrl: '/partials/jobGroupCreate.html',
                    controller: 'jobGroupCreateCtrl'
                }).state('users', {
                    url: '/users',
                    templateUrl: '/partials/users.html',
                    controller: 'usersCtrl'
                }).state("jobView", {
                    url: '/job/:id',
                    templateUrl: '/partials/job.html',
                    controller: 'jobCtrl'
		}).state('jobEdit', {
                    url: '/job/:id/edit',
                    templateUrl: '/partials/jobEdit.html',
                    controller: 'jobEditCtrl'
                }).state('jobsMy', {
                    url: '/me/jobs',
                    templateUrl: '/partials/jobsMy.html',
                    controller: 'jobsMyCtrl'
                }).state('profile', {
                    url: '/me',
                    templateUrl: '/partials/profile.html',
                    controller: 'profileCtrl'
                }).state("userSettings", {
                    url: "/me/settings",
                    templateUrl: '/partials/userSettings.html',
                    controller: 'userSettingsCtrl'
                }).state('accountCredentials', {
                    url: '/account/credentials',
                    templateUrl: '/partials/accountCredentials.html',
                    controller: 'accountCredentialsCtrl'
                });
        /**
         * Default home path
         */
	$urlRouterProvider.otherwise('/home');
        
        /**
         * Enable html 5 pushstate 
         */
	$locationProvider.html5Mode(true);
        
        /**
         * Angular Cache configuration
         * 
         * Configuration for Angular Cache Provider
         * All Restangular models have their own cache providers
         */
        $angularCacheFactoryProvider.setCacheDefaults({
            options: {
                capacity: 10000,
                maxAge: 900000,
                deleteOnExpire: 'aggressive',
                recycleFreq: 600000,
                cacheFlushInterval: 3600000,
                storageMode: 'localStorage',
            }
        });
        /**
         * base url of REST API
         */
        RestangularProvider.setBaseUrl('/api');
        /**
         * Error interceptor for all Restangular Http requests
         * 
         * Whenever it returns false, prevents the promise linked to Restangular request to be executed
         * All others return values are ignored and the promise follows the usual path, eventually reaching the success or error hooks 
         * 
         * @param object res response from http request
         * @param object promise to be returned
         * @returns false or anything else
         */
        RestangularProvider.setErrorInterceptor = function(res, promise) {
            
        };
        
}]);

// Run the application
jobsApp.run(['$rootScope', '$http', function($rootScope, $http) {
        $rootScope.logout = function(){
            window.location = "/logout";
        };
        
        $http.get('/api/init')
                .success(function(res) {
           $rootScope.userDash = res.user;         
        });
}]);