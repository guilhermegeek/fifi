angular.module('jobsApp').factory('postJobRes', function()
{
    var postJobRes = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            id:null,
            name: '',
            content: '',
            categories: []
        });
        angular.extend(this, data);
    };
    return postJobRes;
});