# Models

Models are represented as *Req. In future i'll also apply models for response, and have a provider getting and setting them

Models are factories.

app.factory('postJobRes', function()
{
    var postJobRes = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            id:null,
            name: '',
            content: '',
            categories: []
        });
        angular.extend(this, data);
    };
    return postJobRes;
});

A Service would create a new postJob(response.data) in order to populate it