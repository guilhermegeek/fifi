describe('Welcome tests', function(){
	beforeEach(function(){
		// Call welcome controller
		browser().navigation('/welcome');
		angular.mock.module('jobsApp');
	});

	it('should have welcomeCtrl controller', function(){
		except(jobsApp.welcomeCtrl).toBeDefined();
	});

	it('should have welcomeSvc service', inject['loginSvc', function(loginSvc) {
		except(loginSvc).toBeDefined();
	}]);
});