<?php
use \Slim;
use \Pimple;

use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Fifi\ServiceModel\Dto as Dto;
use Fifi\Business as Business;
use Fifi\Services as Services;
use Fifi\Data as Data;
use Fifi\Infrastructure;
require_once '../bootstrap.php';
$ioc;
global $ioc;



/**
 * API Services 
 */
abstract class ControllerBase {
    protected $app;
    protected $service;
    
    public function __construct(\Pimple $di) {
        $app = $di['$app'];
        $this->init($di);
    }
    
    public abstract function init(\Pimple $di);
}



/**
 * Slim Application
 */
$app = new \Slim\Slim(array(
    'mode' => 'development',
    'http.version' => '1.1'
));
/**
 * IOC
 */
$factory = new \Fifi\ContainerFactory();
$ioc = $factory->create('../');

$app->container->ioc = $ioc;
$auctionCtrl = $ioc['auctionController'];
$ioc['app'] = $app;
$appBoot = new \Fifi\AppBoot();
$appBoot->init();
$ioc['authId'] = function(){
    return \Volupio\AuthService::getUserId();
};

$app->setName('noctulaJobs');

/**
 * Development mode
 * 
 * Used during development and test
 */
$app->configureMode('development', function() use($app) {
   $app->config(array(
       'log.enabled' => true,
       'debug' => true
   ));
});
/**
 * Production mode
 * 
 * Used during production
 */
$app->configureMode('production', function() use($app) {
   $app->config(array(
       'log.enabled' => false,
       'debug' => false
   ));
});

//$app->add(new \Slim\Middleware\ContentTypes());

/**
 * Authentication
 * 
 * This hook is called before the function. $app->method accepts more than a function
 */
$authenticate = function($app) {
    return function() use ($app) {
      if(is_null($_SESSION['auth'])){
          throw new Exception("Not authenticated");
      }
    };
};

/**
 * Interceptor for authentication
 * info
 * Auth intercepts is a Middlware
 */
class AuthInterceptor extends Slim\Middleware{
   public function call(){
       $this->app->hook('slim.before.dispatch', array($this, 'onBeforeDispatch'));
        $authCookie = $this->app->request()->cookies()->get("auth");
        if(!is_null($authCookie) && !empty($authCookie)){
            $authJson = json_decode($authCookie);
            if(is_null($authJson)) {
                $this->next->call();
                return;
            }
            $repo = $this->app->container->ioc['userRepository'];

            
            $auth = $repo->validateAuth($authJson->id, $authJson->token);
            // If auth cookie is invalid, remove it
            if(is_null($auth)) {
                $this->app->response()->cookies()->remove('auth');
            }
            if(!is_null($auth)){
                $_SESSION['auth'] = json_encode($auth);
            }
        } else {
            unset($_SESSION['auth']);
        }
        $this->next->call();
   } 
      public  function is_route_protected($route){
       $privateRoutes = array('home', 'jobs');
       return in_array($route, $privateRoutes);
   }
   public function is_authenticated(){
       return isset($_SESSION['auth']) && !is_null($_SESSION['auth']);
   }
   /**
    * We can't get the current route on call because hasn't been resolved yet
    */
   public function onBeforeDispatch(){
       //$route = $this->app->router()->getCurrentRoute();
       $uri = $_SERVER['REQUEST_URI'];
    if(strpos($uri, '?') !== false){
        $path = substr(substr($uri, 1), 0, strpos(substr($uri, 1), "?"));
    }
    else {
        $path = substr($uri, 1);
    }
    if($this->is_route_protected($path) && !$this->is_authenticated()){
        // redirect  . urlencode(filter_input(INPUT_SERVER, 'REQUEST_URI'))
        $this->app->redirect('/welcome');        
    }
   }
}
$app->add(new AuthInterceptor());

$app->get('/welcome.json', function() use($app) {
    $response = new Dto\WelcomeResponse();
    $response->totalJobsFinished = 21;
    $response->totalRegistered = 155;
    echo json_encode($response);
    
});

/**
 * Creation of a new basic account
 */
$app->post('/register', function() use($app, $ioc) {
 
    $req = json_decode($app->request()->getBody());
    
    if(!property_exists($req, 'firstName') || !property_exists($req, 'lastName') || !property_exists($req,  'email') || !property_exists($req, 'password') || !property_exists($req, 'passwordConfirm')){
        $app->response->status(400);
        $response->apiError('Fields missing', 'NotEmpty');
        echo json_encode($response);
        return;
    }
            
    $res = $ioc['userBusiness']->basicRegistration($req->firstName, $req->lastName, $req->email, $req->password, $req->passwordConfirm); 
    echo json_encode($res);
    
});

/**
 * Login using email and password
 */
$app->post('/login', function() use($ioc, $app) {
    $response = new Dto\PostLoginResponse();
    $req = json_decode($app->request()->getBody());
    if(!property_exists($req, 'email') || !property_exists($req, 'password')){
        $app->response->status(400);
        $response->apiError('Fields missing', 'NotEmpty');
        echo json_encode($response);
        return;
    }
    
    $accountBusiness = $ioc['accountBusiness'];
    $res = $accountBusiness->authenticate($req->email, $req->password);
    if($res->statusCode != 200) {
        $app->response->status(400);
    }
    $app->response()->cookies->set("auth", json_encode($res->auth));
    echo json_encode($res);
});
/**
 * Update Basic Information
 */
$app->post('/api/account/info', function() use($app, $ioc) {
    $id = $ioc['authId'];
    $req = json_decode($app->request->getBody());
    $app->contentType('application/json');
    if(!property_exists($req, 'firstName') || !property_exists($req, 'lastName')){
        $app->status(400);
        $res = new Dto\SaveBasicInfoResponse();
        $res->apiError('Fields missing', 'NotEmpty');
        $app->response()->headers()->set("Content-Type: application/json");
        echo json_encode($res);
        exit;
    }
    $res = $ioc['accountService']->saveBasicInfo($id, $req->firstName, $req->lastName);

    echo json_encode($res);
});

/**
 * Account information
 * 
 * Basic account information
 */
$app->get('/api/account/info', function() use($app, $ioc) {
    $id = $ioc['authId'];
    $ioc['accountService']->getBasicInfo($id);    
});
/**
 * Logout 
 */
$app->get('/logout', function() use($ioc, $app) {
    $app->response()->cookies->remove('auth');
    unset($_SESSION['auth']);
    $app->redirect('/welcome');
});

$app->get('/api/users', function() use($ioc) {
   $userBusiness = $ioc['userBusiness'];
   $users = $userBusiness->find(0, 200);
   echo json_encode($users);
});

$app->post('/api/jobs/:id/info', function($id) use($ioc, $app) {
    $auctionSvc = $ioc['auctionService'];
    $req = json_decode($app->environment['slim.input']);
    $app->contentType('application/json');
    $auctionSvc->putAuctionBasicInfo($ioc['authId'], $id, $req->name, $req->content);
    
});
/*
 * Get Job by Id
 */
$app->get('/api/jobs/:id', function($id) use($ioc, $app) {
    $auctionSvc = $ioc['auctionService'];
    //$app->contentType('application/json');
    $auctionSvc->getAuction($ioc['authId'], $id);
});

/*
 * API Routes 
 * API is acessed from any device with internet from http://noctulajobs.com/api/jobs
 * Because this API implements REST, the request methods affect the operation (Representational State Transfer)
 * GET -> Read
 * POST -> Insert
 * PUT -> Update
 * DELETE -> Remove
 * Ie: PUT /api/job/status - Update the status of a Job
 * Wikipedia about REST API: http://pt.wikipedia.org/wiki/REST
 * This API will be consumed by websites and mobile applications. Even SSH if you want!
 */
$app->get('/api/jobs', function() use ($ioc, $app) {
    $auctionSvc = $ioc['auctionService'];
    if(!is_null($app->request()->get('owner'))){
        //$request->setOwnerId($app->request()->get('owner'));
    }
    $skip = 0;
    $take = 20;
    $minBudget = null;
    $maxBudget = null;
    $begin = null;
    $end = null;
    $categories = array();
    $auctionSvc->findAuctions($skip, $take, $minBudget, $maxBudget, $begin, $end, $categories);
});

$app->get('/api/me/jobs', function() use($ioc, $app) {
    $auctionSvc = $ioc['auctionService'];
    $skip = 0;
    $take = 200;
    
    $auctionSvc->getAuctionsByOwner($ioc['authId'], $skip, $take);
    
});

$app->get('/api/me/groups', function() use($ioc, $app) {
   $auctionSvc = $ioc['auctionService'];
   $skip = 0;
   $take = 200;
   
   $auctionSvc->getGroupsByOwner($ioc['authId'], $skip, $take);
});

/**
 * Create Job
 * Create a single Job or Workspace of Jobs
 */ 
$app->post('/api/jobs', function () use ($app, $ioc) {
    
    $auctionSvc = $ioc['auctionService'];
    $req = json_decode($app->environment['slim.input']);
    $categories = array();
    
    /**
     * Auction with group
     */
    if(property_exists($req, 'groupId')) {
        $response = $auctionSvc->postAuctionWithGroup($ioc['authId'], $req->groupId, $req->name, $req->content, $req->payment, $req->minBudget, $req->maxBudget, $req->begin, $req->end, $categories);
    }
    /**
     * Single auction
     */
    else {
        $response = $auctionSvc->postAuction($ioc['authId'], $req->name, $req->content, $req->payment, $req->minBudget, $req->maxBudget, $req->begin, $req->end, $categories);
    }
    $app->contentType('application/json');
    echo json_encode($response);
});

$app->get('/account/confirm', function() use($app, $ioc) {
   $ioc['accountSvc']->confirmAccount($app->request()->get('id'), $app->request->get('token'));
});

$app->post('/api/group', function() use($app, $ioc) {
    $auctionSvc = $ioc['auctionService'];
    $req = json_decode($app->environment['slim.input']);
    
    if(!property_exists($req, 'name') || !property_exists($req, 'content')){
        $response = new Dto\PostWorkspaceResponse();
        $response->apiError('Fields empty', 'NotEmpty');
        
        $app->status(400);
        return $response;
    }
    
    $response = $auctionSvc->postGroup($ioc['authId'], $req->name, $req->content);
    echo json_encode($response);
    
});

/**
 * PAGES
 */
$app->get('/home.json', function() use($app, $ioc){
    $response = new Dto\HomeResponse();
  
    
    echo json_encode($response);
    //json_encode(array_values($jobs));
});

/**
 * Controllers
 */
$app->get('/api/job/create.json', function() use($ioc, $app) {
    $ioc['auctionController']->getAuctionCreate($ioc['authId'], 0, 200);
});

/*
 * Default AngularJS homepage
 * This route handle all not found requests, wich it's resolved in clientside
 */
$app->get('.*', function () use($app, $ioc){
        include("app.php");
    }
);

$app->run();
