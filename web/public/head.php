<meta charset="UTF-8">
<title>Noctula Jobs</title>
<script type="text/javascript" src="/assets/javascript/jquery.js"></script>
<script type="text/javascript" src="/bower/angular/angular.js"></script>
<script type="text/javascript" src="/bower/angular-sanitize/angular-sanitize.js"></script>
<script type="text/javascript" src="/assets/javascript/angular-route.js"></script>
<script type="text/javascript" src="/assets/javascript/angular-resource.js"></script>
<script type="text/javascript" src="/assets/javascript/angular-animate.js"></script>
<script type="text/javascript" src="/assets/javascript/less.js"></script>
<script type="text/javascript" src="/assets/javascript/ui-bootstrap-tpls.js"></script>
<script type="text/javascript" src="/assets/javascript/ui-map.js"></script>
<script type="text/javascript" src="/bower/angular-ui-router/release/angular-ui-router.js"></script>
<script type="text/javascript" src="/assets/javascript/textAngular.js"></script>

<script type="text/javascript" src="/bower/ng-grid/ng-grid-2.0.7.min.js"></script>
<script type="text/javascript" src="/bower/select2/select2.js"></script>
<script type="text/javascript" src="/bower/angular-ui-select2/src/select2.js"></script>
<script type="text/javascript" src="/bower/lodash/dist/lodash.js"></script>
<script type="text/javascript" src="/bower/restangular/dist/restangular.js"></script>
<script type="text/javascript" src="/bower/angular-cache/dist/angular-cache.js"></script>
<script type="text/javascript" src="/bower/angular-dragdrop/src/angular-dragdrop.js"></script>
<script type="text/javascript" src="/bower/angular-wizard/dist/angular-wizard.js"></script>
<script type="text/javascript" src="/bower/angular-file-upload/angular-file-upload.js"></script>
<script type="text/javascript" src="/bower/textAngular/textAngular.js"></script>
<script type="text/javascript" src="/bower/angular-touch/angular-touch.js"></script>
<script type="text/javascript" src="/app/app.js"></script>
<!-- Dependencies. Next step will be to bundle them all -->
<script type="text/javascript" src="/app/controllers/homeCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobGroupCreateCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobCreateCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobGroupCreateCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobEditCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobIndexCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobViewCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/userSettingsCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/welcomeCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/usersCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/jobsMyCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/faqCtrl.js"></script>
<script type="text/javascript" src="/app/controllers/userProfileCtrl.js"></script>
<script type="text/javascript" src="/app/services/accountSvc.js"></script>
<script type="text/javascript" src="/app/services/jobSvc.js"></script>
<script type="text/javascript" src="/app/services/userSvc.js"></script>
<script type="text/javascript" src="/app/services/homeSvc.js"></script>
<script type="text/javascript" src="/app/services/welcomeSvc.js"></script>
<script type="text/javascript" src="/app/services/modalSvc.js"></script>
<!-- Directives -->
<script type="text/javascript" src="/app/directives/bidCreate.js"></script>
<script type="text/javascript" src="/app/directives/auctionDisplay.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/less/jobs.css" />
<link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css'>
<script src="//platform.linkedin.com/in.js" type="text/javascript">
        api_key: 75czhuuwoydwi7
        authorize: true
      </script>
      <script type="text/javascript">
function onLinkedInAuth() {
  IN.API.Profile("me")
    .result( function(me) {
      var id = me.values[0].id;
      alert(id);
      // AJAX call to pass back id to your server
    });
}
</script>
