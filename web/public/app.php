<!doctype html>
<html lang="en">
<head>
	<?php include_once("head.php"); ?>
</head>
<body ng-app="jobsApp">
    <script type="text/ng-template" id="step.html">
        <section ng-show="selected" ng-class="{current: selected, done: completed}" class="step" ng-transclude>
        </section>    
    </script>
    <script type="text/ng-template" id="wizard.html">
            <div class="clearfix">
             <ul class="steps-indicator steps-{{steps.length}}" ng-if="!hideIndicators">
                <li ng-class="{default: !step.completed && !step.selected, current: step.selected && !step.completed, done: step.completed && !step.selected, editing: step.selected && step.completed}" ng-repeat="step in steps">
                    <a ng-click="goTo(step)">{{step.title}}</a>
                </li>
            </ul>
            <div class="steps" ng-transclude></div>
        </div>
    </script>
    <script type="text/ng-template" id="modalDisplay.html">
         <div class="modal-header">
            <h3>{{title}}</h3>
        </div>
        <div class="modal-body">
            {{body}}
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="ok()">OK</button>
            <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
        </div>        
    </script>
	<?php include_once("header.php"); ?>
	<div ui-view></div>
	<?php include_once("footer.php"); ?>
</body>
</html>