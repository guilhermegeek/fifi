module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('composer.json'),
		phpunit: {
			classes: {
				dir: 'vendor/slim/slim/tests/jobs/'
			},
			options: {
				bin: 'vendor/bin/phpunit',
				colors: true,
				bootstrap: 'vendor/slim/slim/tests/bootstrap.php'
			}
		}
	});
	grunt.loadNpmTasks('grunt-phpunit');
	grunt.registerTask('default', [])
};