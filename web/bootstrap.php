<?php
$vendorDir = __DIR__.'/vendor/';
require $vendorDir . 'autoload.php';

//require_once $vendorDir . ''
/*
require_once $rDir . 'vendor/predis/predis/lib/Predis/Autoloader.php';

require $rDir . 'vendor/slim/slim/Slim/Slim.php';
require $rDir . 'vendor/pimple/pimple/lib/Pimple.php';
require_once $rDir . 'vendor/marc-mabe/php-enum/src/MabeEnum/Enum.php';
require_once $rDir . 'vendor/predis/predis/lib/Predis/Autoloader.php';

require 'vendor/swiftmailer/swiftmailer/lib/swift_required.php';
  */  

if(session_id() == '' || !isset($_SESSION)) {
    // session isn't started
    session_start();
}

/**
 * Auto Loaders
 */
\Slim\Slim::registerAutoloader();
\Predis\Autoloader::register();

/**
 * Doctrine ODM
 */
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;

//AnnotationDriver::registerAnnotationClasses();


spl_autoload_extensions(".php"); // comma-separated list
spl_autoload_register();

$appBoot = new \Fifi\AppBoot();
$appBoot->init();
/*
 * IOC Container Pimple
 * We're using pimpline because it's simple and does the job
 * It's not the same has having Ninject on .NET C#, but it's ok for now :)
 */

/*
 * Registering Dependencies
 * To register a dependency we use pimple dictionary pattern
 * For a class, store a value with class name, and other with the function return the class instance
 * Class JobBusiness ie: 
 *      $ioc['jobBusinessClass'] = 'JobBusiness';
 *      $ioc['jobBusiness'] = function($c) {
 *          return new JobBusiness();
 *      }
 */

interface ServiceContainerInterface {
    public function registerProvider(ServiceProviderInterface $serviceProviderInterface);
    public function terminate();
}

interface ServiceProviderInterface {
    public function register(\Pimple $container);
    public function terminate(\Pimple $container);
}

class ServiceContainer extends \Pimple implements ServiceContainerInterface{
    
    private $providers = array();
    
    public function registerProvider(ServiceProviderInterface $provider) {
        $this->providers[] = $provider;
        $provider->register($this);
    }
    
    public function terminate(){
        foreach($this->providers as $provider) {
            $provider->terminate($this);
        }
    }
}
