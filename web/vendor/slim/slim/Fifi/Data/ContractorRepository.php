<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Fifi\Data;

/**
 * Description of ContractorRepository
 *
 * @author guilherme
 */
class ContractorRepository {
    
    /**
     *
     * @var \DocumentMannager
     */
    private $dm;
    
    public function __construct(\Pimple $ioc) {
        $this->dm = $ioc['dm'];
    }
   /**
     * Push the workspace id in user workspace reference
     * @param ObjectId $userId
     * @param ObjectId $workspaceId
     */
    public function addGroup($userId, $groupId) {
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\User')
                ->update()
                ->field('_id')->equals($userId)
                ->field('groupIds')->push($groupId)
                ->getQuery()
                ->execute();
    }

    /**
     * Add new reference of auction to user
     * @param type $userId
     * @param type $auctionId
     */
    public function addAuction($userId, $auctionId) {
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\User')
                ->update()
                ->field('_id')->equals($userId)
                ->field('auctionIds')->push($auctionId)
                ->getQuery()
                ->execute();
    }
    
    /**
     * A list of groups the user has permissions
     * @param type $userId
     * @return type
     */
    public function getGroupIds ($userId) {
    
        $user = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\User')
                ->field('_id')->equals($userId)
                ->select('groupIds')
                ->getQuery()
                ->getSingleResult();
        return $user->getGroupIds();
    }
    /**
     * A list of groups the user has permissions
     * @param type $userId
     * @return type
     */
    public function getAuctionIds ($userId) {
    
        $user = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\User')
                ->field('_id')->equals($userId)
                ->select('auctionIds')
                ->getQuery()
                ->getSingleResult();
        return $user->getAuctionIds();
    }
}
