<?php

namespace Fifi\Data;

use Fifi\ServiceModel as ServiceModel;
use Fifi\ServiceModel\Domain as Domain;
use Fifi\ServiceModel\Dto as Dto;
use Doctrine\ODM\MongoDB;

/**
 * Data layer for auctions
 *
 * @author Volupio.com
 */
class AuctionRepository extends MongoDB\DocumentRepository {
    
    /**
     * Document Mannager
     * 
     * @var type DocumentMannager 
     */
    protected $dm;
    private $userRepo;
    public function __construct(\Pimple $container) {
        $this->dm = $container['dm'];
        $this->userRepo = $container['userRepository'];
    }
    /**
     * Create auction
     * 
     * @param type $name name used for search and seo porpuses
     * @param type $content content description
     * @param type $minBudget minimum budget
     * @param type $maxBudget maximum budget
     * @param type $begin Begin time of project
     * @param type $end End datetime of project
     * @param array $categories categories of auctions
      */
    public function createAuction($userId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, array $categories) {
        $job = new \Fifi\ServiceModel\Domain\Job();
        $job->setName($name);
        $job->setContent($content);
        $job->setMinBudget($minBudget);
        $job->setMaxBudget($maxBudget);
        $job->setCategories($categories);
        $job->setState(ServiceModel\AuctionState::Created);
        $job->setBegin($begin);
        $job->setEnd($end);
        $job->setPayment($payment);
        $job->setOwnerId($userId);
        $job->setGroup(false);
        $job->setBids(array());
        $job->setBidsCount(0);
        
        $this->dm->persist($job);
        $this->dm->flush();
        
        return $job;
    }
   
    
    /**
     * Create an auction for a existing group
     * 
     * @param type $groupId
     * @param type $name
     * @param type $content
     * @param type $minBudget
     * @param type $maxBudget
     * @param type $begin
     * @param type $end
     * @param array $categories
     */
    public function createAuctionForGroup($userId, $groupId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, array $categories){
        $job = new \Fifi\ServiceModel\Domain\Job();
        $job->setName($name);
        $job->setContent($content);
        $job->setMinBudget($minBudget);
        $job->setMaxBudget($maxBudget);
        $job->setCategories($categories);
        $job->setState(ServiceModel\AuctionState::Created);
        $job->setBegin($begin);
        $job->setEnd($end);
        $job->setPayment($payment);
        $job->setGroupId($groupId);
        $job->setGroup(false);
        $job->setOwnerId($userId);
        $job->setBids(array());
        $job->setBidsCount(0);
        
        $this->dm->persist($job);
        $this->dm->flush();
        
        return $job;
    }
    
  
    
    /**
     * Auction Group
     * 
     * Groups are stored the same collection as Auctions
     * @param type $name
     * @param type $content
     */
    public function createGroup($userId, $name, $content) {
        $j = new Domain\Job();
        $j->setName($name);
        $j->setContent($content);
        $j->jobs = array();
        $j->setOwnerId($userId);
        $j->setGroup(true);
        $j->setBids(array());
        $j->setBidsCount(0);
        
        $this->dm->persist($j);
        $this->dm->flush($j);
        
        return $j;
    }
    
    /**
     * Update Group
     * 
     * @param type $id group id
     * @param type $name
     * @param type $content
     */
    public function saveGroup($id, $name, $content) {
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->update()
                ->field('_id')->equals($id)
                ->field('name')->set($name)
                ->field('content')->set($content)
                ->field('group')->set(true)
                ->getQuery()
                ->execute();
        $this->dm->flush();
    }
    
    /**
     * Push a new auction dto into a existing auction group
     * 
     * @param type $groupId MongoId
     * @param type $auction Auction dto
     */
    public function addAuctionToGroup($groupId, $auction){
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->update()
                ->field('_id')->equals($groupId)
                ->field('jobs')->push($auction)
                ->getQuery()
                ->execute();
      
        $this->dm->flush();
    }
    
    /**
     * Update auction information
     * 
     * @param type $id
     * @param type $name
     * @param type $content
     */
    public function saveAuction($id, $name, $content) {
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->update()
                ->field('_id')->equals($id)
                ->field('name')->set($name)
                ->field('content')->set($content)
                ->getQuery()
                ->execute();
        $this->dm->flush();
    }
    
    /**
     * Get auction by id
     * 
     * @param type $id
     * @return \Fifi\ServiceModel\Domain\Job
     */
    public function get($id) {
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->equals($id)
                ->getQuery()
                ->getSingleResult();
        return $res;
    }
    
    public function getInfo($auctionId) {
        $res = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->equals($auctionId)
                ->select('_id', 'name', 'content')
                ->getQuery()
                ->getSingleResult();
        return $res;
    }
    
    /**
     * Return list of auctions by owner
     * 
     * @param type $userId owner id
     */
    public function getAuctionsByOwner($userId, $skip, $take){
        
        $cursor = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('ownerId')->equals($userId)
                ->field('group')->equals(false)
                ->getQuery()
                ->execute();
        return iterator_to_array($cursor);
    }
    
    /**
     * Return list of groups by owner
     * 
     * @param type $userId
     * @param type $skip
     * @param type $take
     * @return type
     */
    public function getGroupsByOwner($userId, $skip, $take) {
        $cursor = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('ownerId')->equals($userId)
                ->field('group')->equals(true)
                ->getQuery()
                ->execute();
        return iterator_to_array($cursor);
    }
    
    
    public function getByIds($ids) {
        $cursor = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->in($ids)
                ->getQuery()
                ->execute();
        return iterator_to_array($cursor);
    }
    
    /**
     * Query auctions
     * 
     * @param type $skip
     * @param type $take
     * @param type $minBudget
     * @param type $maxBudget
     * @param type $begin
     * @param type $end
     * @param array $categories
     */
    public function findAuctions($skip, $take, $minBudget, $maxBudget, $begin, $end, array $categories){
        $q = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->hydrate(true)
                /*
                 * Return only groups and single auctions
                 * Auctions with group id are already returned by their group
                 */
                ->field('groupId')->equals(null);
        
        /**
         * Filter queries
         */
        if(!is_null($minBudget)){
            $q = $q->field('minBudget')->equals($minBudget);
        }
        if(!is_null($maxBudget)) {
            $q = $q->field('maxBudget')->equals(new \MongoId($maxBudget));
        }
        if(!is_null($begin)) {
            $q = $q->field('begin')->equals(new \MongoId($begin));
        }
        if(!is_null($end)) {
            $q = $q->field('end')->equals(new \MongoId($end));
        }
        
        $jobs = $q->sort('id', 'desc')
                ->select('_id', 'name', 'content', 'minBudget', 'maxBudget', 'begin', 'end', 'jobs', 'owerId', 'state', 'group', 'groupId')
                ->getQuery()
                ->execute();
        $res = $jobs->toArray();
        
        $r = array();
        foreach($res as $j) {
            $d = new Dto\JobDto();
            $d->setId($j->getId());
            $d->setName($j->getName());
            $d->setContent($j->getContent());
            $jobs = $j->getJobs();
            if(count($jobs) > 0) {
                $a = array();
                foreach($jobs as $v) {
                    $dt = new Domain\GroupJob();
                    $dt->setId($v->getId());
                    $dt->setName($v->getName());
                    $dt->setContent($v->getContent());
                    $dt->setMinBudget($v->getMinBudget());
                    $dt->setMaxBudget($v->getMaxbudget());
                    $dt->setBegin($v->getBegin());
                    $dt->setEnd($v->getEnd());
                    array_push($a, $dt);
                }
                $d->setJobs($a);
            }
            $d->setGroup($j->getGroup());
            $d->setGroupId($j->getGroupId());
            array_push($r, $d);
        }
        
        return $r;
    }
    
    /**
     * Return auctions by Group
     * 
     * The auctions in result are up-to-date (they're not the embed documents on the auction group)
     */
    public function findByGroup($groupId) {
        $group = $this->dm->createBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('groupId')->equals($groupId)
                ->field('group')->equals(true)
                ->getQuery()
                ->execute();
        return iterator_to_array($group);
    }
    
    /**
     * Get list of bids
     * 
     * @param type $auctionId
     * @return type
     */
    public function getBids($auctionId) {
        $cursor = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->equals($auctionId)
                ->getQuery()
                ->execute();
        return iterator_to_array($cursor);
    }
    
    /**
     * Create new bid
     * 
     * @param type $userId
     * @param type $auctionId
     * @param type $ammount
     */
    public function createBidForAuction($userId, $auctionId, $ammount)  {
        $bid = new Domain\AuctionBid();
        $bid->setUserId($userId);
        $bid->setAmmount($ammount);
        $r = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->equals($auctionId)
                ->field('bids')->push($bid)
                ->field('bidsCount')->inc(1)
                ->getQuery()
                ->execute();
        $a = $r;
        
    }
    
    /**
     * Get auction state
     * 
     * @param type $auctionId
     * @return type
     */
    public function getAuctionState($auctionId) {
        return $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->equals($auctionId)
                ->select('state')
                ->getQuery()
                ->execute();
    }
    
}
