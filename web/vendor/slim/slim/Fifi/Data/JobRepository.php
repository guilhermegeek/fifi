<?php
namespace Fifi\Data;
use Fifi\ServiceModel\Domain as Domain;
use Fifi\ServiceModel\Dto as Dto;

/**
 * Job data layer
 */
class JobRepository  {
    private $dm;
    private $ioc;
            
    public function __construct(\Pimple $container) {
        $this->dm = $container['dm'];
        $this->ioc = $container;
    }
    /**
     * Create a new Job Group
     * @param type $name
     * @param type $content
     * @return type
     */
    public function add($name, $content){
        $j = new Domain\Job();
        $j->name = $name;
        $j->content = $content;
        $j->jobs = array();
        $j->workspace = true;
        $this->dm->persist($j);
        $this->dm->flush($j);
        
        $this->incrementJobCounter();
        return $j;
    }
    /**
     * Update Jobs references on Group
     */
    public function putGroupJobs($groupId, array $jobIds){
        $this->getDatabase()->job->update(array('_id' => $groupId),
                array('$push' => array('jobIds' => $jobIds)));
    }
    public function getGroupJobIds($groupId) {
        
        return $this->getDatabase()->job->findOne(array('_id' => new \MongoId($groupId), 'workspace' => true));
    }
    /**
     * create new category of job
     * 
     * @param type $name
     */
    public function addCategory($name){
     
    }
    /**
     * Creates a new Job and update reference in Job Group
     * @param type $parent
     * @param type $title
     * @param type $content
     * @param type $minBudget
     * @param type $maxBudget
     */
    public function addJob($parent, $name, $content, $minBudget, $maxBudget, $begin, $end, $payment, $workspace = null) {
     
        $j = new Domain\Job();
        $j->name = $name;
        $j->content = $content;
        $j->minBudget = $minBudget;
        $j->maxBudget = $maxBudget;
        $j->begin = $begin;
        $j->end = $end;
        $j->workspace = false;
        $j->workspaceId = $parent;
        $j->payment = $payment;
                
       
        $this->dm->persist($j);
        $this->dm->flush($j);
     //$this->getDatabase()->job->insert($j);
     $this->incrementJobCounter();
     return $j;
    }
    
    public function addWorkspace($userId, $name, $content) {
        $j = new Domain\Job();
        $j->setName($name);
        $j->setContent($content);
        $jobs = array();
        $j->setJobs($jobs);
        $j->setWorkspace(true);
        $j->setOwnerId($userId);
        
        $this->dm->persist($j);
        $this->dm->flush($j);
        return $j;
    }
    public function getWorkspace($id) {
        $workspace = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job')
                ->field('_id')->equals(new \MongoId($id))
                ->select('_id', 'name', 'content')
                ->getQuery()
                ->getSingleResult();
        return $workspace;
    }
    /**
     * Find Jobs
     * @param type $workspace
     * @return type
     */
    public function find($workspace = null, $ownerId = null) {
        /*$m = new \Mongo;
        $c = $this->getDatabase()->selectCollection('job');
        $ops = $c->aggregate(array(
            array(
                '$project' => array (
                    'name' => 1,
                    'workspaceId' => 1,
                )
            ),
            array('$unwind' => 'workspaceId'),
            array(
                '$group'=> array(
                    '_id' => array('_id' => '$workspaceId')
                    )
                )
        ));
        return iterator_to_array($ops);*/

        $q = $this->dm->createQueryBuilder('Fifi\ServiceModel\Domain\Job');
        if(!is_null($workspace)){
            $q = $q->field('workspaceId')->equals($workspace);
        }
        if(!is_null($ownerId)) {
            $q = $q->field('ownerId')->equals(new \MongoId($ownerId));
        }
        $jobs = $q->getQuery()
                ->execute();
        return iterator_to_array($jobs);
    }
  
    public function get($id) {
        //$job = $this->getDatabase()->job->findOne(array('_id' => new \MongoId($id)));
        return $this->dm->getRepository('Fifi\ServiceModel\Domain\Job')->find($id);
        //return $job;
    }
    public function getJobCounter(){
        $client = new \Predis\Client();
        $counter = $client->get('jobs::counter');
        return $counter;
    }
    private function incrementJobCounter(){
        $client = $this->ioc['redis'];
        $client->incr('jobs::counter');
    }
}
