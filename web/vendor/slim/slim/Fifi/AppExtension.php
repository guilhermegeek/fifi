<?php
/**
 * IOC Container Setup
 */
namespace Fifi;

use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Aura\Intl\Package;

class AppExtension { 
    /**
     * Register IOC dependencies
     * 
     * This extension is also used for tests
     * @param \Pimple $ioc
     * @param \Pimple $ioc
     * @return type
     */
    public function register(\Pimple $ioc) {
        
        /**
         * Controllers
         */
        $ioc['auctionController'] = function($c) use($ioc) {
          return new \Fifi\Controllers\AuctionController($ioc);  
        };
        
        /*
         * Services
         */
        $ioc['accountSvc'] = function($c) use($ioc) {
            return new \Fifi\Services\AccountService($ioc);  
        };
        $ioc['jobBusiness'] = function ($c) use($ioc) {
            return new \Fifi\Business\JobBusiness($ioc);
        };
        $ioc['jobRepository'] = function($c) use($ioc) {  
            return  new \Fifi\Data\JobRepository($c);
        };
        $ioc['auctionService'] = function($ioc) {
            return new \Fifi\Services\AuctionService($ioc);
        };
        $ioc['auctionBusiness'] = function($c) use($ioc) {
            return new \Fifi\Business\AuctionBusiness($c);
        };
        $ioc['auctionRepository'] = function($c) use($ioc) {
            return new \Fifi\Data\AuctionRepository($c);
        };
        
        $ioc['accountService'] = function($c) use($ioc) {
          return new \Fifi\Services\AccountService($c);
        };
       
        $ioc['bidderBusiness'] = function($c) use($ioc) {
            return new \Fifi\Business\BidderBusiness($c);
        };
        $ioc['bidderRepository'] = function($c) use($ioc) {
            return new \Fifi\Data\BidderRepository($c);
        };
        $ioc['contractorBusiness'] = function($c) use($ioc) {
            return new \Fifi\Business\ContractorBusiness($c);
        };
        $ioc['contractorRepository'] = function($c) use($ioc) {
            return new \Fifi\Data\ContractorRepository($c);
        };
        $ioc['redis'] = function($c) use($ioc) {
          return new \Predis\Client();  
        };
        $ioc['dm'] = function($c)  {
            
            AnnotationDriver::registerAnnotationClasses();
            
            //$client = new \MongoClient('localhost', array('db' => 'jobs' , 'username' => 'guilherme', 'password' => 'dumbpw'));
            $connection = new Connection();
            $config = new Configuration();
            $config->setProxyDir(__DIR__ . '/Proxies');
            $config->setProxyNamespace('Proxies');
            $config->setHydratorDir(__DIR__ . '/Hydrators');
            $config->setHydratorNamespace('Hydrators');
            $config->setAutoGenerateProxyClasses(true);
            $config->setAutoGenerateHydratorClasses(true);
            $config->setDefaultDB('jobs');
            $config->setMetadataDriverImpl(AnnotationDriver::create(array('Fifi/ServiceModel/Domain/', 'Volupio/Domain')));

            $dm = DocumentManager::create($connection, $config);
            return $dm;
        };
    }
}