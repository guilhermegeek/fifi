<?php
namespace Fifi\Services;

    abstract class ServiceBase{
        public $app;
        public $ioc;
        
        
        public function __construct(\Pimple $ioc) {
          // $this->$app = $ioc['app'];
           $this->init($ioc);
        }
        
        /**
         * Initialize service
         * 
         * Method to be implemented by all services
         */
        public abstract function init(\Pimple $ioc);
       
    }
