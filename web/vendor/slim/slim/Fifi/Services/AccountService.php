<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Fifi\Services;

/**
 * Description of AccountService
 *
 * @author guilherme
 */
class AccountService extends ServiceBase {
    
    /**
     *
     * @var \Volupio\Business\AccountBusiness
     */
    private $userBusiness;

    
    public function init(\Pimple $ioc) {
        $this->userBusiness = $ioc['userBusiness'];
      
    }
    
    public function basicRegistration($firstName, $lastName, $email, $password, $passwordConfirm) {
        $res = new \Fifi\ServiceModel\BasicRegistrationResponse();
        
        try {
            $this->userBusiness->basicRegistration($firstName, $lastName, $email, $password, $passwordConfirm);
        }
        catch(\Volupio\Exceptions\EmailInvalidException $ex) {
            $res->apiError('Invalid email confirmation', 'InvalidEmail');
            return $res;
        }
        catch(\Volupio\Exceptions\EmailAlreadyInUseException $ex) {
            $res->apiError('Email already in use', 'EmailInUse');
            return $res;
        }
        catch(\Exception $ex) {
            $res->error('internal');
            return $res;
        }
        return $res;
    }
    
    public function login($email, $password){
        
    }
    public function getBasicInfo($id) {
        $res = $this->userBusiness->getBasicInfo($id);
        echo json_encode($res);
    }
    public function saveBasicInfo($id, $firstName, $lastName) {
        $res = $this->userBusiness->saveBasicInfo($id, $firstName, $lastName);
        echo json_encode($res);
    }
    
    /**
     * Confirm a account registration
     * 
     * @param type $userId
     * @param type $token
     */
    public function confirmAccount($userId, $token) {
       $res = $this->userBusiness->confirmRegister($userId, $token);
       echo json_encode($res);
    }
}
