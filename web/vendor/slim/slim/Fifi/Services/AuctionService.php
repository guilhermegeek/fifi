<?php
namespace Fifi\Services;
use Fifi\ServiceModel\Dto as Dto;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuctionService
 *
 * @author guilherme
 */
class AuctionService extends ServiceBase {
    /**
     *
     * @var \Fifi\Business\AuctionBusiness
     */
    private $auctionBusiness;
    
    public function init(\Pimple $ioc) {
        $this->auctionBusiness = $ioc['auctionBusiness'];
    }
    /**
     *  Creates a new group
     * 
     * @param type $userId
     * @param type $name
     * @param type $content
     */
    public function postGroup($userId, $name, $content) {
        $res = $this->auctionBusiness->createGroup($userId, $name, $content);
        echo json_encode($res);
    }
    
    /**
     * Get group by id
     * 
     * @param type $userId
     * @param type $groupId
     */
    public function getGroup($userId, $groupId) {
        $res = $this->auctionBusiness->getGroup($userId, $groupId);
        echo json_encode($res);
    }
    
    public function getAuction($userId, $auctionId) {
        $res = $this->auctionBusiness->getAuction($userId, $auctionId);
        echo json_encode($res);
    }
    
    public function postAuction($userId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, $categories) {
        $res = $this->auctionBusiness->createAuction($userId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, $categories);
        echo json_encode($res);
    }
    
    public function postAuctionWithGroup($userId, $groupId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, $categories) {
        $res = $this->auctionBusiness->createAuctionForGroup($userId, $groupId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, $categories);
        echo json_encode($res);
    }
    
    public function getAuctionBasicInfo($userId, $auctionId) {
        $res = $this->auctionBusiness->getAuctionInfo($userId, $auctionId);
        echo json_encode($res);
    }
    
    public function putAuctionBasicInfo($userId, $auctionId, $name, $content) {
        $res = $this->auctionBusiness->saveAuctionInfo($userId, $auctionId, $name, $content);
        echo json_encode($res);
    }
    
    public function putAuctionToGroup($groupId, $auctionId){
        $res = $this->auctionBusiness->addAuctionToGroup($groupId, $auctionId);
        echo json_encode($res);
    }
    
    public function findAuctions($skip, $take, $minBudget, $maxBudget, $begin, $end, $categories) {
        $res = $this->auctionBusiness->getAuctions($skip, $take, $minBudget, $maxBudget, $begin, $end, $categories);
        echo json_encode($res);
    }
    
    public function getAuctionsByOwner($ownerId, $skip, $take) {
        $res = $this->auctionBusiness->getAuctionsByOwner($ownerId, $skip, $take);
        echo json_encode($res);
    }
    
    public function getGroupsByOwner($ownerId, $skip, $take) {
        $res = $this->auctionBusiness->getGroupsByOwner($ownerId, $skip, $take);
        echo json_encode($res);
    }
}
