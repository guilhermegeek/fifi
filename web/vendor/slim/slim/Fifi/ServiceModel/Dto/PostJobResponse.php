<?php

namespace Fifi\ServiceModel\Dto;

class PostJobResponse extends \Volupio\Infrastructure\ResponseStatus {
    public $jobId;
}
