<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Fifi\ServiceModel\Dto;

class SaveAuction {
    /** 
      * @ODM\Id 
      */
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;;
    }
    
    /**
     * Owner Id
     * 
     * @ODM\ObjectId
     */
    public $ownerId;
    public function getOwnerId(){
        return $this->ownerId;
    }
    public function setOwnerId($ownerId){
        $this->ownerId = $ownerId;
    }
    
    
    /**
     * @ODM\Field(type="string")
     */
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    /**
     * @ODM\Field(type="string")
     */
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }

    /**
     * Indicates if Job is a Workspace 
     * @ODM\Field(type="boolean")
    */
    public $workspace;
    public function getWorkspace(){
        return $this->workspace;
    }
    public function setWorkspace($workspace) {
        $this->workspace = $workspace;
    }
    
    /**
     * Parent workspace id
     * @ODM\Field(type="string")
     * @var type 
     */
    public $workspaceId;
    public function getWorkspaceId(){
        return $this->workspaceId;
    }
    public function setWorkspaceId($id) {
        $this->workspaceId = $id;
    }
        
    /**
     * Jobs if Workspace
     * @ODM\Field(type="hash")
     * @var type 
     */
    public $jobs;
    public function getJobs(){
        return $this->jobs;
    }
    public function setJobs($jobs) {
        $this->jobs = $jobs;
    }
    
    /**
     * Minimum budget
     * @var decimal;
     * @ODM\Field(type="float")
     */
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget) {
        $this->minBudget = $minBudget;
    }
    /**
     * Maximum Budget
     * @ODM\Field(type="float")
     * @var decimal
     */
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget) {
        $this->maxBudget = $maxBudget;
    }
    
    
    
    /**
     * Payments methods available
     */
    public $payment;
    public function getPayment(){
        return $this->payment;
    }
    public function setPayment($payment) {
        $this->payment = $payment;
    }
}

