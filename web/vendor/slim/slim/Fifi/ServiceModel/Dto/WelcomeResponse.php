<?php
namespace Fifi\ServiceModel\Dto;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WelcomeDto
 *
 * @author guilherme
 */
class WelcomeResponse {
    /**
     * Counter of registered users
     * @var type 
     */
    public $totalRegistered;
    
    /**
     * Counter of jobs finished
     * @var type 
     */
    public $totalJobsFinished;
}
