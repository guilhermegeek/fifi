<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Fifi\ServiceModel\Dto;
/**
 * Description of JobSubDto
 *
 * @author guilherme
 */
class JobSubDto {
    public $id;
    public $thumbnailSrc;
    
    public $name;
    public $content;
    public $excerpt;
    public $minBudget;
    public $maxBudget;
    public $begin;
    public $end;
    public $payment;
    public $state;
    public $bids;
}