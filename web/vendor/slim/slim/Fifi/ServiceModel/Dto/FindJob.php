<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Fifi\ServiceModel\Dto;

/**
 * Description of FindJob
 *
 * @author guilherme
 */
class FindJob {
    private $ownerId = null;
    public function getOwnerId(){
        return $this->ownerId;
    }
    public function setOwnerId($ownerId) {
        $this->ownerId = $ownerId;
    }
    public function hasOwnerId(){
        return !is_null($this->ownerId);
    }
}
