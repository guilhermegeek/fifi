<?php
namespace Fifi\ServiceModel\Dto;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GetGroupResponse
 *
 * @author Volupio.com
 */
class GetGroupResponse {
    
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }
            
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }
    
    public $auctions;
    public function getAuctions(){
        return $this->auctions;
    }
    public function setAuctions($auctions) {
        $this->auctions = $auctions;
    }
}
