<?php
namespace Fifi\ServiceModel\Dto;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CreateAuction {
  
    /**
     * Owner Id
     * 
     */
    public $ownerId;
    public function getOwnerId(){
        return $this->ownerId;
    }
    public function setOwnerId($ownerId){
        $this->ownerId = $ownerId;
    }
    
    
    /**
     * Name
     */
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    /**
     * Content
     */
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }
    
    /**
     * Parent workspace id
     * @var type 
     */
    public $workspaceId;
    public function getWorkspaceId(){
        return $this->workspaceId;
    }
    public function setWorkspaceId($id) {
        $this->workspaceId = $id;
    }
        
    /**
     * Jobs if Workspace
     * @var type 
     */
    public $jobs;
    public function getJobs(){
        return $this->jobs;
    }
    public function setJobs($jobs) {
        $this->jobs = $jobs;
    }
    
    /**
     * Minimum budget
     * @var decimal
     */
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget) {
        $this->minBudget = $minBudget;
    }
    /**
     * Maximum Budget
     * @var decimal
     */
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget) {
        $this->maxBudget = $maxBudget;
    }
    
    /**
     * Begin Date
     * @var date
     */
    public $begin;
    public function getBegin(){
        return $this->begin;
    }
    public function setBegin($begin) {
        $this->begin = $begin;
    }
    
    /**
     * End Date
     * @var date
     */
    public $end;
    public function getEnd(){
        return $this->end;
    }
    public function setEnd($end) {
        $this->end = $end;
    }
    
    /**
     * Job state
     * @var type 
     */
    public $state;
    public function getState(){
        return $this->state;
    }
    public function setState($state) {
        $this->state = $state;
    }

    /**
     * Categories for the Job
     */
    public $categories;
    public function getCategories(){
        return $this->categories;
    }
    public function setCategories($categories) {
        $this->categories = $categories;
    }
    
    /**
     * Questions the candidate must answer
     */
    public $questions;
    public function getQuestions(){
        return $this->questions;
    }
    public function setQuestions($questions) {
        $this->questions = $questions;
    }
    
    /**
     * Payments methods available
     */
    public $payment;
    public function getPayment(){
        return $this->payment;
    }
    public function setPayment($payment) {
        $this->payment = $payment;
    }
}