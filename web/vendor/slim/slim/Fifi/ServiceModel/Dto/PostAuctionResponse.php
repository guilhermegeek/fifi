<?php
namespace Fifi\ServiceModel\Dto;

/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */

/**
 * Description of PostAuctionResponse
 *
 * @author Volupio.com
 */
class PostAuctionResponse extends \Volupio\Infrastructure\ResponseStatus {
    /**
     * Group Id
     * @var type MongoId
     */
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * Group name
     * @var type string
     */
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Content
     * @var type string 
     */
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }
    
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget){
        $this->minBudget = $minBudget;
    }
    
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget){
        $this->maxBudget = $maxBudget;
    }
    
    public $begin;
    public function getBegin(){
        return $this->begin;
    }
    public function setBegin($begin) {
        $this->begin = $begin;
    }
    
    public $end;
    public function getEnd(){
        return $this->end;
    }
    public function setEnd($end) {
        $this->end = $end;
    }
}
