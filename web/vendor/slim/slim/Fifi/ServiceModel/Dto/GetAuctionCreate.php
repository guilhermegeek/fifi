<?php

/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */
namespace Fifi\ServiceModel\Dto;

/**
 * Description of GetAuctionCreate
 *
 * @author guilherme
 */
class GetAuctionCreate {
    
    /**
     * Auction Groups
     * @var array
     */
    public $groups;
    public function getGroups(){
        return $this->groups;
    }
    public function setGroups($groups) {
        $this->groups = $groups;
    }
}
