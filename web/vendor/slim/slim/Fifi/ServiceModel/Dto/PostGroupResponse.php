<?php

namespace Fifi\ServiceModel\Dto;

class PostGroupResponse extends \Volupio\Infrastructure\ResponseStatus{
    /**
     * Group Id
     * @var type MongoId
     */
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    
    /**
     * Group name
     * @var type string
     */
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Content
     * @var type string 
     */
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }
}