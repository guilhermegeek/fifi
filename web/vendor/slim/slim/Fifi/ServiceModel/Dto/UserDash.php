<?php

namespace Fifi\ServiceModel\Dto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDash
 *
 * @author Volupio
 */
class UserDash {
    private $avatarUri;
    public function getAvatarUri(){
        return $this->avatarUri;
    }
    public function setAvatarUri($avatarUri) {
        $this->avatarUri = $avatarUri;
    }
    
    private $displayName;
    public function getDisplayName(){
        return $this->displayName;
    }
    public function setDisplayName($displayName) {
        $this->displayName = $displayName;
    }
    
    public $profileUri;
    public function getProfileUri(){
        return $this->profileUri;
    }
    public function setProfileUri($profileUri) {
        $this->profileUri = $profileUri;
    }
}
