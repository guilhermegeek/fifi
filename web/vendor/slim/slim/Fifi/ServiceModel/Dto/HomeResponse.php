<?php
namespace Fifi\ServiceModel\Dto;

/**
 * DTO for /home response
 */
class HomeResponse extends \Volupio\Infrastructure\ResponseStatus {
    public $jobs;
    public function getJobs(){
        return $this->jobs;
    }
    public function setJobs($jobs) {
        $this->jobs = $jobs;
    }
}
