<?php
namespace Fifi\ServiceModel\Dto;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InitResponse
 *
 * @author guilherme
 */
class InitResponse {
    //put your code here
    
    private $userDash;
    public function setUserDash($userDash) {
        $this->userDash = $userDash;
    }
    public function getUserDash(){
        return $this->userDash;
    }
    
    private $isAuthenticated;
    public function setIsAuthenticated($isAuthenticated) {
        $this->isAuthenticated = $isAuthenticated;
    }
    public function getIsAuthenticated(){
        return $this->isAuthenticated;
    }
}
