<?php
namespace Fifi\ServiceModel\Dto;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * DTO for embed job in groups
 *
 * @author Volupio.com
 */
class GroupJobRef {
    //put your code here
    
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget) {
        $this->minBudget = $minBudget;
    }
    
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget){
        $this->maxBudget = $maxBudget;
    }
}
