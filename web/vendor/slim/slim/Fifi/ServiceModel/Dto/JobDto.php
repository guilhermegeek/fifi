<?php
namespace Fifi\ServiceModel\Dto;

class JobDto {
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content) {
        $this->content = $content;
    }
    
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget){
        $this->minBudget = $minBudget;
    }
    
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget) {
        $this->maxBudget = $maxBudget;
    }
    
    public $begin;
    public function getBegin(){
        return $this->begin;
    }
    public function setBegin($begin) {
        $this->begin = $begin;
    }
    
    public $end;
    public function getEnd(){
        return $this->end;
    }
    public function setEnd($end) {
        $this->end = $end;
    }
    
    public $payment;
    public function getPayment(){
        return $this->payment;
    }
    public function setPayment($payment) {
        $this->payment = $payment;
    }
    
    public $state;
    public function getState(){
        $this->state;
    }
    public function setState($state) {
        $this->state = $state;
    }
    
    public $jobs;
    public function getJobs(){
        return $this->jobs;
    }
    public function setJobs($jobs) {
        $this->jobs = $jobs;
    }
    
    /**
     *
     * @var type 
     * @ODM\Field(type="string")
     */
    public $groupId;
    public function getGroupId(){
        return $this->groupId;
    }
    public function setGroupId($groupId) {
        $this->groupId = $groupId;
    }
    
    /**
     * @ODM\Field(type="boolean")
     * @var type 
     */
    public $group;
    public function getGroup(){
        return $this->group;
    }
    public function setGroup($group) {
        $this->group = $group;
    }
}
