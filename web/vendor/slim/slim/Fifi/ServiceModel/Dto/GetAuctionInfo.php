<?php

/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */
namespace Fifi\ServiceModel\Dto;

/**
 * Description of GetAuctionBasicInfo
 *
 * @author guilherme
 */
class GetAuctionInfo {
   
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    
    public $name;
    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content) {
        $this->content = $content;
    }
}
