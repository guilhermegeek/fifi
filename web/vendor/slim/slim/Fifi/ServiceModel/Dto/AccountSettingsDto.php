<?php
namespace Fifi\ServiceModel\Dto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountSettingsDto
 *
 * @author guilherme
 */
class AccountSettingsDto extends \Volupio\Infrastructure\ResponseStatus {
    public $firstName;
    public $lastName;
    public $birthday;
}
