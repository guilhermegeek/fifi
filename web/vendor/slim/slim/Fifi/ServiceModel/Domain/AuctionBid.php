<?php
/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */
namespace Fifi\ServiceModel\Domain;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * Description of AuctionBid
 *
 * @author guilherme
 * @ODM\EmbeddedDocument
 */
class AuctionBid {
    
    public $userId;
    public function getUserId(){
        return $this->userId;
        
    }
    public function setUserId($userId) {
        return $this->userId;
    }
    
    public $ammount;
    public function getAmmount(){
        return $this->ammount;
    }
    public function setAmmount($ammount) {
        $this->ammount = $ammount;
    }
}
