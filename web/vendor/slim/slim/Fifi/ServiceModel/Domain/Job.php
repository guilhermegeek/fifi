<?php
namespace Fifi\ServiceModel\Domain;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Job
 * 
 * Application Domain or Database Entities
 * Represents the database collection and a domain in Application
 * 
 * @ODM\Document(collection="job")
 */
class Job {
    
    public function __construct(){
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
        
    }
    
     /** 
      * @ODM\Id 
      */
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;;
    }
    
    /**
     * Owner Id
     * 
     * @ODM\ObjectId
     */
    public $ownerId;
    public function getOwnerId(){
        return $this->ownerId;
    }
    public function setOwnerId($ownerId){
        $this->ownerId = $ownerId;
    }
    
    
    /**
     * @ODM\Field(type="string")
     */
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    /**
     * @ODM\Field(type="string")
     */
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }

    /**
     * Indicates if Job is a Workspace 
     * @ODM\Field(type="boolean")
    
    public $workspace;
    public function getWorkspace(){
        return $this->workspace;
    }
    public function setWorkspace($workspace) {
        $this->workspace = $workspace;
    }
    */
    /**
     * Parent workspace id
     * @ODM\Field(type="string")
     * @var type 
     
    public $workspaceId;
    public function getWorkspaceId(){
        return $this->workspaceId;
    }
    public function setWorkspaceId($id) {
        $this->workspaceId = $id;
    }
      */  
    /**
     * Jobs if Workspace
     * 
     * @ODM\EmbedMany(targetDocument="GroupJob", strategy="set")
     * @var type 
     */
    public $jobs = array();
    public function getJobs(){
        return $this->jobs;
    }
    public function setJobs($jobs) {
        $this->jobs = $jobs;
    }
    
    /**
     * Minimum budget
     * @var decimal;
     * @ODM\Field(type="float")
     */
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget) {
        $this->minBudget = $minBudget;
    }
    /**
     * Maximum Budget
     * @ODM\Field(type="float")
     * @var decimal
     */
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget) {
        $this->maxBudget = $maxBudget;
    }
    
    /**
     * Begin Date
     * @ODM\Field(type="date")
     * @var date
     */
    public $begin;
    public function getBegin(){
        return $this->begin;
    }
    public function setBegin($begin) {
        $this->begin = $begin;
    }
    
    /**
     * End Date
     * @ODM\Field(type="date")
     * @var date
     */
    public $end;
    public function getEnd(){
        return $this->end;
    }
    public function setEnd($end) {
        $this->end = $end;
    }
    
    /**
     * Job state
     * @var type 
     */
    public $state;
    public function getState(){
        return $this->state;
    }
    public function setState($state) {
        $this->state = $state;
    }

    /**
     * Categories for the Job
     */
    public $categories;
    public function getCategories(){
        return $this->categories;
    }
    public function setCategories($categories) {
        $this->categories = $categories;
    }
    
    /**
     * Questions the candidate must answer
     * @ODM\Field(type="hash")
     */
    public $questions;
    public function getQuestions(){
        return $this->questions;
    }
    public function setQuestions($questions) {
        $this->questions = $questions;
    }
    
    /**
     * Payments methods available
     */
    public $payment;
    public function getPayment(){
        return $this->payment;
    }
    public function setPayment($payment) {
        $this->payment = $payment;
    }
    
    /**
     *
     * @var type 
     * @ODM\Field(type="string")
     */
    public $groupId;
    public function getGroupId(){
        return $this->groupId;
    }
    public function setGroupId($groupId) {
        $this->groupId = $groupId;
    }
    
    /**
     * @ODM\Field(type="boolean")
     * @var type 
     */
    public $group;
    public function getGroup(){
        return $this->group;
    }
    public function setGroup($group) {
        $this->group = $group;
    }
    
    /**
     *
     * @var array
     * @ODM\EmbedMany(targetDocument="AuctionBid", strategy="set")
     */
    public $bids = array();
    public function getBids(){
        return $this->bids;
    }
    public function setBids($bids) {
        $this->bids = $bids;
    }
    
    /**
     * Bids counter
     * @var int
     * @ODM\Field(type="int")
     */
    public $bidsCount;
    public function getBidsCount(){
        return $this->bidsCount;
    }
    public function setBidsCount($bidsCount) {
        $this->bidsCount = $bidsCount;
    }
}