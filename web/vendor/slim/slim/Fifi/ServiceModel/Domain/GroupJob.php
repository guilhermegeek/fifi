<?php

/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */
namespace Fifi\ServiceModel\Domain;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Description of GroupJob
 *
 * @author guilherme
 * @ODM\EmbeddedDocument
 */
class GroupJob {
    
     /** 
      * @ODM\Id 
      */
    public $id;
    public function getId(){
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;;
    }
    
    /**
     * @ODM\Field(type="string")
     */
    public $name;
    public function getName(){
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * @ODM\Field(type="string")
     */
    public $content;
    public function getContent(){
        return $this->content;
    }
    public function setContent($content){
        $this->content = $content;
    }
    
    
    /**
     * Minimum budget
     * @var decimal;
     * @ODM\Field(type="float")
     */
    public $minBudget;
    public function getMinBudget(){
        return $this->minBudget;
    }
    public function setMinBudget($minBudget) {
        $this->minBudget = $minBudget;
    }
    /**
     * Maximum Budget
     * @ODM\Field(type="float")
     * @var decimal
     */
    public $maxBudget;
    public function getMaxBudget(){
        return $this->maxBudget;
    }
    public function setMaxBudget($maxBudget) {
        $this->maxBudget = $maxBudget;
    }
    
    /**
     * Begin Date
     * @ODM\Field(type="date")
     * @var date
     */
    public $begin;
    public function getBegin(){
        return $this->begin;
    }
    public function setBegin($begin) {
        $this->begin = $begin;
    }
    
    /**
     * End Date
     * @ODM\Field(type="date")
     * @var date
     */
    public $end;
    public function getEnd(){
        return $this->end;
    }
    public function setEnd($end) {
        $this->end = $end;
    }
    
    /**
     * Job state
     * @var type 
     */
    public $state;
    public function getState(){
        return $this->state;
    }
    public function setState($state) {
        $this->state = $state;
    }

    /**
     * Categories for the Job
     */
    public $categories;
    public function getCategories(){
        return $this->categories;
    }
    public function setCategories($categories) {
        $this->categories = $categories;
    }
    
    /**
     * Questions the candidate must answer
     * @ODM\Field(type="hash")
     */
    public $questions;
    public function getQuestions(){
        return $this->questions;
    }
    public function setQuestions($questions) {
        $this->questions = $questions;
    }
    
    /**
     * Payments methods available
     */
    public $payment;
    public function getPayment(){
        return $this->payment;
    }
    public function setPayment($payment) {
        $this->payment = $payment;
    }
}
