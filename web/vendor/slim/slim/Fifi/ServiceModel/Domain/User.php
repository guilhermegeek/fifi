<?php
namespace Fifi\ServiceModel\Domain;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * user domain available for Fifi
 * 
 * Users have types:
 *  - Worker - Person who candidate to auction jobs/services
 *  - Employer - Company or individual person who's contracting and aucting in Fifi
 * @ODM\Document(collection="users")
 */
class User  {
    
    public function __construct() {
        //$this->groupIds = array();
    }
    
   /**
     * 
     * @ODM\Id()
     */
    public $id;
    public function setId($id){
        $this->id = $id;;
    }
    public function getId(){
        return $this->id;
    }
    
    /**
     * @ODM\Field(type="string")
     * @var type 
     */
    public $state;
    public function setState($state) {
        $this->state = $state;
    }
    public function getState(){
        return $this->state;
    }
    
    /**
     * first name
     * 
     * First name is concatenate with LastName
     * @ODM\Field(type="string")
    */
    public $firstName;
    public function setFirstName($firstName){
        $this->firstName = $firstName;
    }
    public function getFirstName(){
        return $this->firstName;
    }
    /**
     * lastname
     * 
     * Lastname is concatenate with FirstName
     * @ODM\Field(type="string")
     */
    public $lastName;
    public function setLastName($lastName){
        $this->lastName = $lastName;
    }
    public function getLastName(){
        return $this->lastName;
    }
    /**
     * primary email
     * 
     * Used for authentication, not necessarely the registered email used
     * Recover password and others sensitive mails will use this
     * 
     * @ODM\Field(type="string")
     */
    public $email;
    public function setEmail($email){
        $this->email = $email;
    }
    public function getEmail(){
        return $this->email;
    }
    /**
     * basic authentication password
     * 
     * Password hash
     * @ODM\Field(type="string")
     */
    public $password;
    public function setPassword($password){
        $this->password = $password;
    }
    public function getPassword(){
        return $this->password;
    }
    
    /**
     * Workspaces
     * 
     * @ODM\Field(type="hash")
     */
    public $groupIds;
    public function getGroupIds(){
        return $this->groupIds;
    }
    public function setGroupIds($ids){
        $this->groupIds = $ids;
    }
    
    /**
     * Auctions
     * 
     * @ODM\Field(type="hash")
     */
    public $auctionIds;
    public function getAuctionIds(){
        return $this->auctionIds;
    }
    public function setAuctionIds($ids) {
        $this->auctionIds = $ids;
    }
    
    /**
     * jobs the user has done in Fifi
     * 
     * @ODM\Field(type="hash")
     * @var type 
     */
    public $jobsDoneIds;
    public function setJobsDoneIds($ids) {
        $this->jobsDoneIds = $ids;
    }
    public function getJobsDoneIds(){
        return $this->jobsDoneIds;
    }
    
    /**
     * jobs the user published in Fifi and were finished
     * @ODM\Field(type="hash")
     * @var type 
     */
    public $jobsPublishedIds;
    public function setJobsPublishedIds($ids) {
        $this->jobsPublishedIds = $ids;
    }
    public function getJobsPublishedIds(){
        return $this->jobsPublishedIds;
    }
    
    /**
     *jobs that haven't been published yet
     * @ODM\Field(type="hash") 
     * @var type 
     */
    public $jobsDraftsIds;
    public function setJobsDraftsIds($ids){
        $this->jobsDraftsIds = $ids;
    }
    public function getJobsDraftsIds(){
        return $this->jobsDraftsIds;
    }
}