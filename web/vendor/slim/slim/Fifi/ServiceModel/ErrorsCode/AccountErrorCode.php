<?php
namespace Fifi\ServiceModel\ErrorsCode;
use MabeEnum\Enum;


/*
 * API Error Code
 * This enumeration holds all error codes returned from API
 */

class AccountErrorCode extends Enum {
    const RegisterSucefull = 1;
    const RegisterEmailAlreadyRegistered = 2;
    const RegisterEmailInvalid = 2;
    const RegisterPasswordNotMatching = 3;
    const LoginEmailOrPasswordIncorrect = 4;
    const LoginAccountDisabled = 5;
    const LoginAccountNotConfirmed = 6;
    const RecoverPasswordEmailDontExist = 7;
    // Password and Confirm password don't match
    const ChangePasswordNotMatching = 8;
    const ChangePasswordInvalidPassword = 9;
}