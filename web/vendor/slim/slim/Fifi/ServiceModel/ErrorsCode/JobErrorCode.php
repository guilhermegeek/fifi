<?php
namespace Fify\ServiceModel\ErrorsCode;
use MabeEnum\Enum;

class JobErrorCode extends Enum {
    const PostJobNoJobs = 1;
}