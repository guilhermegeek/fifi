<?php

/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */
namespace Fifi\ServiceModel;
use MabeEnum\Enum;
/**
 * Description of AuctionState
 *
 * @author Volupio.com
 */
class AuctionState extends Enum {
    const Created = 1;
    const Published = 2;
    const Completed = 3;
}
