<?php
namespace Fifi;

class ContainerFactory {
    /**
     * Create Container
     * 
     * @param string $configDir path of config.json 
     * @return \Pimple
     */
    public function create($configDir) {
        $container = new \Pimple();
        $extension = new \Fifi\AppExtension();
        $extension->register($container);
        
        $extension = new \Fifi\ConfigExtension($configDir . '/config.json');

        $extension->register($container);
        
        $vExtension = new \Volupio\AppTestExtension();
        $vExtension->register($container);
        
        
        return $container;
    }
}