<?php
namespace Fifi;

interface ExtensionInterface {
    function register(\Pimple $container);
}