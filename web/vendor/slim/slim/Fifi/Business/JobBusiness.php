<?php
namespace Fifi\Business;
use Doctrine\ODM\MongoDB\DocumentManager ;
use Fifi\ServiceModel\Domain as Domain;
use Fifi\ServiceModel\Dto as Dto;
use Fifi\ServiceModel\ErrorsCode as ErrorsCode;
use Fifi\Data as Data;

/**
 * Business Layers
 */

class JobBusiness {
    /**
     * Document Mapper
     */
 
    private $ioc;
    private $jobRepository;
    
    public function __construct(\Pimple $container) {
        $this->ioc = $container;
        $this->jobRepository = $container['jobRepository'];

    }
    
   /**
    * Find a list of available Jobs
    * @return array
    */
    public function find(Dto\FindJob $request = null){
       
        $response = array();
        
        $jobs = $this->ioc['jobRepository']->find(null, $request->getOwnerId());
        
        foreach($jobs as $job) {
            $dto = new Dto\JobDto();
            $dto->id = (string)$job['_id'];
            $dto->name = $job['name'];
            $dto->content = $job['content'];
            
            if(in_array('begin', $job))
                $dto->begin = $job['begin'];
            
            if(in_array('end', $job))
                $dto->end = $job['end'];
            
            if(in_array('minBudget', $job))
                $dto->minBudget = $job['minBudget'];
            
            if(in_array('maxBudget', $job))
                $dto->maxBudget = $job['maxBudget'];
            
            if(in_array('payment', $job))
                $dto->payment = $job['payment'];
            
            array_push($response, $dto);
        }
        return $response;
    }
    public function get($id){
        $repo = new $this->ioc['jobRepository'];
        $jobDto = $repo->getById($id);
        return $jobDto;
    }
    
    public function getCategories($name) {
        
    }
    
    public function addWorkspace($userId, $name, $content) {
        $repo = $this->ioc['jobRepository'];
        $response = new Dto\PostWorkspaceResponse();
        
        return $response;
    }
    
    public function createGroup($userId, $name, $content) {
        
    }
   
    
    /**
     * Create a new Job
     * @param type $title
     * @param type $description
     * @param array $jobs
     * @return \Dto\PostJobResponse
     */
    public function add($title, $description, array $jobs){
        $repo = new \Fifi\Data\JobRepository($this->ioc);
        $response = new Dto\PostJobResponse();

        /*
         * Validate if user is creating at least one job
         */
        if(count($jobs) == 0){
            $response->apiError("You have to create at least one job", ErrorsCode\JobErrorCode::PostJobNoJobs );
            return $response;
        }
        
        $jobGroup = $repo->add($title, $description);
        $response->jobId = $jobGroup->id;
        $jobIds = array();
        foreach($jobs as $job) {
            $j = $this->addJob($jobGroup->id, $job->name, $job->content, $job->minBudget, $job->maxBudget, $job->begin, $job->end, $job->payment);
            array_push($jobIds, $j->id);
        }
        // Update Group jobs property
        $repo->putGroupJobs($jobGroup->id, $jobIds);
        $response->success('Auction is pendending for approval.');
        
        return $response;
        
    }
    
    /**
     * Private helper to add a new Job to Group
     * @param type $parent
     * @param type $title
     * @param type $description
     * @param type $minBudget
     * @param type $maxBudget
     */
    private function addJob($parent, $name, $content, $minBudget, $maxBudget, $begin, $end, $payment) {
        $repo = $this->ioc['jobRepository'];
        $job = $repo->addJob($parent, $name, $content, $minBudget, $maxBudget, $begin, $end, $payment);
        return $job;
    }
}