<?php
namespace Fifi\Business;
use Fifi\ServiceModel\Domain as Domain;
use Fifi\ServiceModel\Dto as Dto;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuctionBusiness
 *
 * @author Volupio.com
 */
class AuctionBusiness {
    
    /**
     *
     * @var \Fifi\Data\AuctionRepository
     */
    private $auctionRepo;
    
    /**
     *
     * @var \Fifi\Data\ContractorRepository
     */
    private $contractorRepository;
    
    /**
     *
     * @var \Fifi\Data\BidderRepository
     */
    private $bidderRepository;
    
    /**
     *
     * @var \Fifi\Data\UserRepository
     */
    private $userRepo;
    
    public function __construct($ioc) {
        $this->auctionRepo = $ioc['auctionRepository'];
        $this->userRepo = $ioc['userRepository'];
        $this->contractorRepository = $ioc['contractorRepository'];
        $this->bidderRepository = $ioc['bidderRepository'];
    }
    
    /**
     * Return a list of auctions
     * 
     * @param type $skip
     * @param type $take
     * @param type $minBudget
     * @param type $maxBudget
     * @param type $begin
     * @param type $end
     * @param type $categories
     * @return type
     */
    public function getAuctions($skip, $take, $minBudget, $maxBudget, $begin, $end, array $categories) {
        $res = $this->auctionRepo->findAuctions($skip, $take, $minBudget, $maxBudget, $begin, $end, $categories);
        $a = array();
        foreach($res as $r) {
            array_push($a, $r);
        }
        return $a;
    }
    
    /**
     * Get all groups owned by the user
     * 
     * @param type $userId
     * @param type $skip
     * @param type $take
     */
    public function getGroupsByOwner($userId, $skip, $take) {
        $ids = $this->contractorRepository->getGroupIds($userId);
        $res = $this->auctionRepo->getByIds($ids);
        $a = array();
        foreach($res as $r) {
            array_push($a, $r);
        }
        return $a;
    }
    
    /**
     * Return list of auctions owned by user
     * 
     * @param type $userId
     * @param type $skip
     * @param type $take
     * @return type
     */
    public function getAuctionsByOwner($userId, $skip, $take){
        $ids = $this->contractorRepository->getAuctionIds($userId);
        $res = $this->auctionRepo->getByIds($ids);
        //$res = $this->auctionRepo->getAuctionsByOwner($userId, $skip, $take);
        $a = array();
        foreach($res as $r) {
            array_push($a, $r);
        }
        return $a;
    }
    
    /**
     * Return a auction by id
     * 
     * @param type $userId
     * @param type $auctionId
     * @return \Fifi\ServiceModel\Dto\GetAuctionResponse
     */
    public function getAuction($userId, $auctionId) {
       $response = new Dto\GetAuctionResponse();
       
       $auction = $this->auctionRepo->get($auctionId);
       
       $response->setId($auction->getId());
       $response->setName($auction->getName());
       $response->setContent($auction->getContent());
       $response->setPayment($auction->getPayment());
       $response->setBegin($auction->getBegin());
       $response->setEnd($auction->getEnd());
       $response->setMinBudget($auction->getMinBudget());
       $response->setMaxBudget($auction->getMaxBudget());
       $response->setOwnerId($auction->getOwnerId());
       return $response;
    }
    
    /**
     * Get Group by Id
     * 
     * @param type $userId
     * @param type $groupId
     * @return \Fifi\ServiceModel\Dto\GetGroupResponse
     */
    public function getGroup($userId, $groupId) {
        $response = new Dto\GetGroupResponse();

        $group = $this->auctionRepo->get($groupId);
        $response->setId($group->getId());
        $response->setName($group->getName());
        $response->setContent($group->getContent());
        $response->setAuctions($group->getJobs());
        return $response;
    }
    
    /**
     * Add a existing auction to a group
     * 
     * @param type $groupId
     * @param type $auctionId
     * @return \Fifi\ServiceModel\Dto\PutAuctionGroup
     */
    public function addAuctionToGroup($groupId, $auctionId) {
        $response = new Dto\PutAuctionGroup();
        $group = $this->auctionRepo->get($groupId);
        $dto = new \Fifi\ServiceModel\Domain\GroupJob();
        $dto->setId($group->getId());
        $dto->setName($group->getName());
        $dto->setMinBudget($group->getMinBudget());
        $dto->setMaxBudget($group->getMaxBudget());
        $this->auctionRepo->addAuctionToGroup($groupId, $dto);
        return $response;
    }
    
    /**
     * Create new group for auctions
     * 
     * @param type $userId MongoId
     * @param type $name
     * @param type $content
     * @return \Fifi\ServiceModel\Dto\PostGroupResponse
     */
    public function createGroup($userId, $name, $content) {
        try {
            $response = new Dto\PostGroupResponse();
           
            $group = $this->auctionRepo->createGroup($userId, $name, $content);

            // Update User Jobs
            $this->contractorRepository->addGroup($userId, $group->getId());

            $response->setId($group->getId());
            $response->setName($group->getName());
            $response->setContent($group->getContent());
            $response->success("Job sucefully created. Thank you sir!");
        } catch(ValidationException $ex) {
            
        }
        return $response;
    }
    
    /**
     * Create new auction
     * 
     * @param type $userId
     * @param type $name
     * @param type $content
     * @param type $payment
     * @param type $minBudget
     * @param type $maxBudget
     * @param type $begin
     * @param type $end
     * @param array $categories
     * @return \Fifi\ServiceModel\Dto\PostAuctionResponse
     */
    public function createAuction($userId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, array $categories) {
        $response = new Dto\PostAuctionResponse();
        try {
            $auction = $this->auctionRepo->createAuction($userId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, $categories);
            
            $this->contractorRepository->addAuction($userId, $auction->getId());
            
            $response->setId($auction->getId());
            $response->setName($auction->getName());
            $response->setContent($auction->getContent());
            $response->setMinBudget($auction->getMinBudget());
            $response->setMaxBudget($auction->getMaxBudget());
            $response->setBegin($auction->getBegin());
            $response->setEnd($auction->getEnd());
            $response->success('Sucefully created!');
            
            return $response;
            
        } catch (Exception $ex) {

        }
    }
    
    /**
     * Create new auction for existing Group
     * 
     * @param type $userId
     * @param type $groupId
     * @param type $name
     * @param type $content
     * @param type $payment
     * @param type $minBudget
     * @param type $maxBudget
     * @param type $begin
     * @param type $end
     * @param array $categories
     * @return \Fifi\ServiceModel\Dto\PostAuctionForGroupResponse
     */
    public function createAuctionForGroup($userId, $groupId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, array $categories) {
        $response = new Dto\PostAuctionForGroupResponse();
        try {
            $auction = $this->auctionRepo->createAuctionForGroup($userId, $groupId, $name, $content, $payment, $minBudget, $maxBudget, $begin, $end, $categories);
            
            $this->addAuctionToGroup($groupId, $auction->getId());
            
            $response->setId($auction->getId());
            $response->setName($auction->getName());
            $response->setContent($auction->getContent());
            $response->setMinBudget($auction->getMinBudget());
            $response->setMaxBudget($auction->getMaxBudget());
            $response->setBegin($auction->getBegin());
            $response->setEnd($auction->getEnd());
            return $response;
            
        } catch (Exception $ex) {

        }
    }
    
    /**
     * Create a new bid for a auction
     * 
     * @param type $userId
     * @param type $auctionId
     * @param type $ammount
     */
    public function createBidForAuction($userId, $auctionId, $ammount)  {
        $response = new Dto\PostAuctionBid();
        
        $this->auctionRepo->createBidForAuction($userId, $auctionId, $ammount);
        
        return $response;
    }
    
    public function getAuctionInfo($userId, $auctionId) {
        $response = new Dto\GetAuctionInfo();
        
        $res = $this->auctionRepo->getInfo($auctionId);
        $response->setId($res->getId());
        $response->setName($res->getName());
        $response->setContent($res->getContent());
       
        return $response;
    }
    
    /**
     * Save auction basic information
     * 
     * @param type $userId
     * @param type $auctionId
     * @param type $name
     * @param type $content
     * @return boolean
     */
    public function saveAuctionInfo($userId, $auctionId, $name, $content)
    {
        $this->auctionRepo->saveAuction($auctionId, $name, $content);
        return true;
    }    
    /**
     * Get list of bids
     * 
     * @param type $userId
     * @param type $auctionId
     * @return array
     */
    public function getBids($userId, $auctionId) {
        $bids = $this->auctionRepo->getBids($auctionId);
        $bidsArray = array();
        foreach($bids as $bid) {
            $b = new Dto\AuctionBidDto();
            $b->setAmmount($bid['ammount']);
            $b->setUserId($bid['userId']);
            array_push($bidsArray, $b);
        }
        return $bidsArray;
    }
}
