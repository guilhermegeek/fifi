<?php

/*
 * Copyright Volupio.com
 * Developed and maitained by Volupio
 */
namespace Fifi\Controllers;
use Fifi\ServiceModel\Dto as Dto;

/**
 * Description of AuctionController
 *
 * @author guilherme
 */
class AuctionController {
    /**
     * Auction Business
     * 
     * @var \Fifi\Business\AuctionBusiness
     */
    private $auctionBusiness;
    
    public function __construct(\Pimple $ioc) {
        $this->auctionBusiness = $ioc['auctionBusiness'];
    }
    
    /**
     * Controller for Auction Create
     * 
     * @param type $userId
     * @param type $skip
     * @param type $take
     */
    public function getAuctionCreate($userId, $skip, $take){
        $response = new Dto\GetAuctionCreate();
        
        $groups = $this->auctionBusiness->getGroupsByOwner($userId, $skip, $take);
        
        $response->groups = $groups;
        echo json_encode($response);
    }
}
