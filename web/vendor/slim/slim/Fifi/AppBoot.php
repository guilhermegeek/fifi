<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Fifi;
/**
 * Description of AppBoot
 *
 * @author guilherme
 */
class AppBoot {
    
    /**
     * Initial configuration for I18N and etc
     */
    public function init(){
       
       /**
        * I18N 
        */
       $language = "en_US";
       putenv("LANG=" . $language);
       putenv("LANGUAGES=" . $language);
       setlocale(LC_ALL, $language);

       /* 
        * Text Domain 
        */
       $domain = "messages";
       $a = bindtextdomain($domain, "/home/guilherme/workspace/fifi/src/I18N/Locale");
       bind_textdomain_codeset($domain, 'UTF-8');

       textdomain($domain);
    }
}
