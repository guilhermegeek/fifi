<?php
use \Fifi\ServiceModel\Domain as Domain;
use \Fifi\ServiceModel as ServiceModel;
use \Fifi\Business as Business;
use \Fifi\Data as Data;

class JobTest extends Base_TestCase {
    
    private $jobBusiness;
    private $jobRepository;
    private $userRepository;
    
    public function __construct(){
        parent::__construct();
        $this->jobBusiness = $this->container['jobBusiness'];
        $this->jobRepository = $this->container['jobRepository'];
        $this->userRepository = $this->container['userRepository'];
    }
    
    
    /** 
     * GET /api/jobs
  
    public function testGetJobs(){
        $jobBusiness = $this->container['jobBusiness'];
        $this->createJob();
        $jobs = $jobBusiness->find();
        $this->assertGreaterThan(0, count($jobs));
    }
    
    public function testGetJobsByWorkspace(){
        throw new Exception("not implemented");
    }
   */
    /**
     * GET /api/jobs/:id
    
    public function testGetJobById() {
        $jobBusiness = $this->container['jobBusiness'];
        $job = $this->createJob();
        $jobDto = $jobBusiness->get($job['_id']);
        $this->assertEquals($job['name'], $jobDto->name);
    }
     */
    /**
     * Removes an sub Job
     * Main Job remains active 
     
    public function testJobCancelSub(){
        throw new Exception("Not implemented");
    }
    */
    /**
     * Accepts an Proposal
     
    public function testJobAcceptProposal(){
        throw new Exception("Not implemented");
    }
    public function testPostWorkspace(){
        $user = $this->mockAuthTest();
        $response = $this->jobBusiness->addWorkspace($user->getId(), 'name', 'new content');
        $this->assertEquals($response->statusCode, 200);
        
        $workspace = $this->jobRepository->getWorkspace($response->workspaceId);
        
        $this->assertEquals('name', $workspace->getName());
        $this->assertEquals(true, $workspace->getWorkspace());
        
        // User Workspaces
        
        //$u = $this->container['dm']->getRepository('Fifi\ServiceModel\Domain\User')->findOneBy(array('_id' => $user->id));
        $this->container['dm']->refresh($user);
        $this->assertEquals(true, in_array($workspace->getId(), $user->getWorkspaceIds()));
    }
     
     */
    /**
     * Create a new Job. A Job group is created even if only one job
     
    public function testPostJob(){
        $jobBusiness = $this->container['jobBusiness'];
        $jobRepository = $this->container['jobRepository'];
        $jobs = array();
        
        $j = new ServiceModel\Dto\JobDto();
        $j->name="Test Auction";
        $j->content = "Hi, this is a test!";
        $j->begin = new DateTime('2014-04-30');
        $j->end = new DateTime('2014-05-30');
        $j->minBudget = 200;
        $j->maxBudget = 2000;
        $j->payment = 'VISA';
        array_push($jobs, $j);
        array_push($jobs, $j);
        array_push($jobs, $j);

        $response = $jobBusiness->add('workspace title', 'workspace description', $jobs);
        $newJob = $jobRepository->get($response->jobId);
        
        $abc = $this->container['dm']->refresh($newJob);
        
        $this->assertEquals($newJob->name, 'workspace title');
        
        // Test if job group has jobs ids
        $group = $jobRepository->getGroupJobIds($response->jobId);
        $this->assertEquals($group['name'], 'workspace title');
        //$this->assertArrayHasKey($newJob['jobIds'][0], $jobs, $message)
    }
    */
    /**
     * Update jobs basic information
    
    public function testPutJob(){
        throw new Exception("Not implemented");
    }
     */
    /**
     * Publish a Job
     * It's here when users are notified
 
    public function testPublishJob(){
        throw new Exception("Not implemented");    
    }
        */
    /**
     * Cancel a Job
     * In case bids were proposed, users are notified
     */
    public function testCancelJob(){
        throw new Exception("Not implemented");
    }
    /**
     * Post a like on a Job 
     */
    public function testPostLikeJob(){
        throw new Exception("Not implemented");
    }
    
    /**
     * User subscrive to Job channel for notifications
     */
    public function testWatchJob(){
    
    }
    
    /**
     * User unsubscrive from Job channnel
     */
    public function testUnwatchJob(){
        throw new Exception("Not implemented");
    }
    
    
    /**
     * Helper method to create Jobs for tests
 
    private function createJob(){
        throw new Exception("Not implemented");
        $repo = new Data\JobRepository();
        $job = $repo->add('test', 'ole');
        return $job;
    }    */
}
?>