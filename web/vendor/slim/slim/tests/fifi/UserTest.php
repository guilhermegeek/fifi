<?php
use Fifi\ServiceModel\Domain as Domain;

class UserTest extends Base_TestCase{
    private $userBusiness;
    private $userRepository;
    
    public function __construct() {
        parent::__construct();
        $this->userBusiness = $this->container['userBusiness'];
        $this->userRepository = $this->container['userRepository'];
    }
    /**
     * Get a user by id
     */
    public function testGetProfile(){
        throw new Exception("Not implemented");
    }
    
    /**
     * Get a list of users
     */
    public function testGetUsers(){
        $this->createUser();
        
        $users = $this->userBusiness->find(0, 50);
        $this->assertTrue(count($users) > 0);
    }
    
    /**
     * Save user privacy settings
     */
    public function testSavePricacy(){
        throw new Exception("Not implemented");
    }
    /**
     * Save user settings
     */
    public function testSaveSettings(){
        throw new Exception("Not implemented");
    }
    
    /**
     * Update user prefered categories
     */
    public function testUpdateCategories(){
        throw new Exception("Not implemented");
    }
    
    private function createUser(){
        $user = new Domain\User();
        $user->setEmail(\Volupio\Business\CommonBusiness::getHash() . '@msn.com');
        $user->setFirstName('Carlos');
        $user->setLastName('Manuel');
        $user->setPassword('123');
        $user->setState(\Volupio\ServiceModel\AccountState::Confirmed);
    }
}
