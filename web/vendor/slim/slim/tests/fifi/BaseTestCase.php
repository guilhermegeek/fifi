<?php
/*
 * Auto loads
 */
abstract class Base_TestCase extends PHPUnit_Framework_TestCase {
    public $container;
    /**
     *
     * @var \Fifi\Data\ContractorRepository
     */
    public $contractorRepository;
    /**
     *
     * @var \Fifi\Data\BidderRepository;
     */
    public $bidderRepository;
    
    public function __construct() {
         $factory = new \Fifi\ContainerFactory();
        $this->container = $factory->create('../../../../../../');
        $app->container->ioc = $this->container;
        $this->bidderRepository = $this->container['bidderRepository'];
        $this->contractorRepository = $this->container['contractorRepository'];
                        
    }
    
    public function request($method, $path, $options = array())
    {
        // Capture STDOUT
        ob_start();

        // Prepare a mock environment
        \Slim\Environment::mock(array_merge(array(
            'REQUEST_METHOD' => $method,
            'PATH_INFO' => $path,
            'SERVER_NAME' => 'slim-test.dev',
        ), $options));

        $app = new \Slim\Slim();
        $this->app = $app;
        $this->request = $app->request();
        $this->response = $app->response();
        $factory = new \Fifi\ContainerFactory();
        $this->container = $factory->create('/home/guilherme/workspace/fifi/src/');
        $app->container->ioc = $this->container;
        // Return STDOUT
        
        return ob_get_clean();
    }
    

    public function get($path, $options = array())
    {
        $this->request('GET', $path, $options);
    }
    public function post($path, $data, $options = array()) {
        $options['slim.input'] = $data;
        $this->request('POST', $path, $options);
    }
    
    /**
     * Mocks a new registration for tests porpuses
     * @return User
     */
    public function mockAuthTest(){
        $repo = $this->container['userRepository'];
        $password = \Volupio\Business\CommonBusiness::getHash();
        $email = \Volupio\Business\CommonBusiness::getHash() . '@msn.com';
        $user = $repo->create('new', 'new', $email, md5($password));
        return $user;
    }
}