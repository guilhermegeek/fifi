<?php

class RoutingTest extends PHPUnit_Framework_TestCase {
    /**
     * Test to extract base segment path
     * This is a fix until i figure out how to real deal with Slim routing
     */
    public function testGetRoute(){
        $uri = '/home?XDEBUG_SESSION_START=netbeans-xdebug';
        if(strpos($uri, '?') !== false){
            $path = substr(substr($uri, 1), 0, strpos(substr($uri, 1), "?"));
        }
        else {
            $path = substr($uri, 1);
        }
        $this->assertEquals('home', $path);
    }
}