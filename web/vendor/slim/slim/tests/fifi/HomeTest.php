<?php
use Fifi\ServiceModel\Dto as Dto;
use Fifi\ServiceModel\Domain as Domain;


class HomeTest extends Base_TestCase{
    
    function testGetHome()  {
        $this->request('GET', '/home');
        $this->assertEquals($this->response->statusCode, 200);
        
    }
}