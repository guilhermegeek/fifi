<?php
use Fifi\ServiceModel\Domain as Domain;
use Fifi\ServiceModel\ErrorsCode as ErrorsCode;

class ProposalTest extends Base_TestCase{
    
    /**
     * Send a proposal to a Job
     */
    public function testPostProposalToJob(){
        throw new Exception("Not implemented");
    }
    
    /**
     * Send a proposal to a Job Group
     */
    public function testPostProposalToJobGroup(){
        throw new Exception("Not implemented");
    }
    
    /**
     * Updates basic information of a proposal
     */
    public function testPutProposal(){
        throw new Exception("Not implemented");
    }
    
    /**
     * Cancels a proposal
     */
    public function testRemoveProposal(){
        throw new Exception("Not implemented");
    }
}