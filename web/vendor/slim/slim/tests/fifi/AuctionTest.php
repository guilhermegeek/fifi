<?php

use Fifi\ServiceModel\Domain as Domain;
use Fifi\ServiceModel\Dto as Dto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuctionTest
 *
 * @author Volupio
 */
class AuctionTest extends Base_TestCase {

    /**
     * Auction Service
     * 
     * @var \Fifi\Services\AuctionService
     */
    private $auctionSvc;

    /**
     * Auction Repository
     * 
     * @var \Fifi\Data\AuctionRepository
     */
    private $auctionRepo;

    /**
     * User Repository
     * 
     * @var \Fifi\Data\UserRepository
     */
    private $userRepo;

    /**
     *
     * @var \Fifi\Business\AuctionBusiness
     */
    private $auctionBus;

    /**
     * Auth DTO
     * @var type 
     */
    private $auth;

    /**
     * 
     * @var DocumentMannager
     */
    private $dm;
    

    public function __construct() {
        parent::__construct();
        $this->auctionRepo = $this->container['auctionRepository'];
        $this->auctionSvc = $this->container['auctionService'];
        $this->auctionBus = $this->container['auctionBusiness'];
        $this->userRepo = $this->container['userRepository'];
        $this->auth = $this->mockAuthTest();
        $this->dm = $this->container['dm'];
    }

    /**
     * Create a new group for Auctions
     */
    public function testCreateGroup() {
        $res = $this->auctionBus->createGroup($this->auth->getId(), 't', 'a');

        $group = $this->auctionRepo->get($res->getId());
        // Compare auctions content
        $this->assertEquals($res->getContent(), $group->getContent());
    }

    public function testCreateAuction() {
        $auctionName = 'asdasdasd';
        $res = $this->auctionBus->createAuction($this->auth->getId(), $auctionName, '$contentasdasdasd', null, null, null, null, null, array());
        $id = $res->getId();
        $auction = $this->auctionRepo->get($id);
        $this->assertEquals($auction->getName(), $auctionName);
    }

    public function testCreateAuctionForGroup() {
        $group = $this->createGroup();
        $this->assertEquals(0, count($group->getJobs()));
        $res = $this->auctionBus->createAuctionForGroup($this->auth->getId(), $group->getId(), '$nameasasdasd', '$contentasdadasd', 'money', 20, 40, new DateTime('2014-01-01'), new DateTime('2014-02-02'), array());
        $auction = $this->auctionRepo->get($res->getId());
        $groupUpdated = $this->auctionRepo->get($group->getId());

        $this->dm->refresh($groupUpdated);
        $this->assertEquals($res->getName(), $auction->getName());
        $jobs = $groupUpdated->getJobs();
        $this->assertGreaterThan(0, count($jobs));
    }

    public function testGetAuctionsByOwner() {
        $this->createAuction();
        $u = $this->userRepo->get($this->auth->getId());
        $this->dm->refresh($u);
        $auctions = $this->auctionBus->getAuctionsByOwner($this->auth->getId(), 0, 500);

        $this->assertGreaterThan(0, count($auctions));
    }

    public function testGetGroupsByOwner() {
        $this->createGroup();
        $u = $this->userRepo->get($this->auth->getId());
        $this->dm->refresh($u);
        $groups = $this->auctionBus->getGroupsByOwner($this->auth->getId(), 0, 500);
        $this->assertGreaterThan(0, count($groups));
    }

    /**
     * Get a group
     */
    public function testGetGroup() {
        $group = $this->createAuction();
        $res = $this->auctionBus->getGroup($this->auth->getId(), $group->getId());
        $this->assertEquals($group->getName(), $res->getName());
    }

    public function testGetGroups() {
        $group = $this->createauction();
    }

    public function testGetAuctions() {
        $this->createAuction();
        $a = array();
        $res = $this->auctionBus->getAuctions(0, 200, null, null, null, null, $a);
        $this->assertTrue(count($res) > 0);
    }

    public function testCreateBid() {
        $auction = $this->createAuction();
        $this->auctionBus->createBidForAuction($this->auth->getId(), $auction->getId(), 123);
        $bids = $this->auctionRepo->getBids($auction->getId());
        $this->assertGreaterThan(0, count($bids));
    }

    public function testGetAuctionInfo() {
        $auction = $this->createAuction();
        $res = $this->auctionBus->getAuctionInfo($this->auth->getId(), $auction->getId());
        $this->assertEquals($auction->getName(), $res->getName());
    }

    public function testSaveAuctionInfo() {
        $auction = $this->createAuction();
        $newName = 'asdasdasd';

        $res = $this->auctionBus->saveAuctionInfo($this->auth->getId(), $auction->getId(), $newName, $auction->getContent());

        $auctionUpdated = $this->auctionRepo->get($auction->getId());
        $this->dm->refresh($auctionUpdated);
        $this->assertEquals($auctionUpdated->getName(), $newName);
    }

    public function testGetAuction() {
        $auction = $this->createAuction();
        $res = $this->auctionBus->getAuction($this->auth->getId(), $auction->getId());
        $this->assertEquals($auction->getName(), $res->getName());
    }

    
    public function testRandomOne(){
        $group = $this->createGroup();
        
    }
    
    /**
     * Create a new auction
     * @return \Fifi\ServiceModel\Domain\Job
     */
    private function createAuction() {
        $j = new Domain\Job();
        $j->setName('asdadasdasd');
        $j->setContent('asdasdasdasd');
        $j->setOwnerId($this->auth->getId());
        $j->setState(\Fifi\ServiceModel\AuctionState::Created);
        $j->setGroup(false);
        $j->setMinBudget(200);
        $j->setMaxBudget(300);
        $j->setBegin(new DateTime('2014-01-01'));
        $j->setEnd(new DateTime('2014-02-01'));
        $j->setState(\Fifi\ServiceModel\AuctionState::Published);

        $this->container['dm']->persist($j);
        $this->container['dm']->flush($j);

        $this->contractorRepository->addAuction($this->auth->getId(), $j->id);

        return $j;
    }
    
    /**
     * Create a new auction group
     * 
     * @return \Fifi\ServiceModel\Domain\Job
     */
    private function createGroup() {
        $j = new Domain\Job();
        $j->setName('$nameasdasd');
        $j->setContent('$contenasdasdasd');
        $j->setGroup(true);
        $j->setState(\Fifi\ServiceModel\AuctionState::Created);
        $j->setJobs(array());
        $j->setOwnerId($this->auth->getId());
        $j->setMinBudget(0);
        $j->setMaxBudget(0);
        $j->setBegin(new DateTime('2014-01-01'));
        $j->setEnd(new DateTime('2014-02-01'));
        $j->setState(\Fifi\ServiceModel\AuctionState::Published);

        $this->container['dm']->persist($j);
        $this->container['dm']->flush($j);

        $this->contractorRepository->addGroup($this->auth->getId(), $j->id);

        return $j;
    }

}
